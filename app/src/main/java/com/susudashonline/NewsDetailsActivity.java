package com.susudashonline;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.VideoView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by vendetta on 28.08.14.
 */
public class NewsDetailsActivity  extends ActionBarActivity {

    private FeedItem feed;
    ProgressDialog pd;
    String baseUrl;
    WebView web;
    String toWeb;



    private String currentUrl;


    public void onRestart(Bundle savedInstanceState){
        super.onRestart();
        if (savedInstanceState != null)
            ((WebView)findViewById(R.id.webView)).restoreState(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_detail);
        web = (WebView)findViewById(R.id.webView);

        if (savedInstanceState != null)
            ((WebView)findViewById(R.id.webView)).restoreState(savedInstanceState);

        feed = (FeedItem) this.getIntent().getSerializableExtra("feed");
        String loading = getResources().getString(R.string.loading);
        String pleaseWait = getResources().getString(R.string.pleaseWait);

        pd = ProgressDialog.show(NewsDetailsActivity.this, pleaseWait, loading, true);
        pd.setCancelable(false);
        baseUrl = getResources().getString(R.string.baseUrl);



        if (null != feed) {

            Spanned htmlSpan = Html.fromHtml(feed.getUrl(), null, null); //(feed.getContent(),p,null)


            toWeb = feed.getUrl();
            Log.w("toWeb", "" + toWeb);
            //  web.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            web.getSettings().setJavaScriptEnabled(true);
            web.getSettings().setAllowFileAccess(true);
            web.getSettings().setPluginState(WebSettings.PluginState.ON);
            web.addJavascriptInterface(NewsDetailsActivity.this,toWeb);
            web.getSettings().setDefaultFontSize(21);
            web.setBackgroundColor(Color.parseColor("#FF2d1816"));
//            web.setBackgroundColor(0);
          //  web.setBackgroundResource(R.drawable.fon1);
            //      web.getSettings().setUseWideViewPort(true);
            //       web.getSettings().setLoadWithOverviewMode(true);


            new DownloadHtmlSource().execute(toWeb);
            final Activity activity = this;
         /*   web.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    // Activities and WebViews measure progress with different scales.
                    // The progress meter will automatically disappear when we reach 100%
                    activity.setProgress(progress * 1000);


                }

            }); */

            web.setWebChromeClient(new WebChromeClient(){
                Activity a;

                public void onShowCustomView(View view, CustomViewCallback callback) {
                    super.onShowCustomView(view, callback);
                    if (view instanceof FrameLayout){
                        FrameLayout frame = (FrameLayout) view;
                        if (frame.getFocusedChild() instanceof VideoView){
                            VideoView video = (VideoView) frame.getFocusedChild();
                            frame.removeView(video);
                            a.setContentView(video);
                            video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mediaPlayer) {
                                    Log.w("complete","");
                                }
                            });
                            video.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                                @Override
                                public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                                    return false;
                                }
                            });
                            video.start();
                        }
                    }
                }
            });

            web.setWebViewClient(new mWebViewClient());

        }
    }



    private void shareContent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, feed.getTitle() + "\n" + feed.getUrl());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share using"));

    }
    @Override
    public void onBackPressed() {

        this.finish();

    }

    private class mWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            pd.show();
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            pd.dismiss();
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.contains("vk.com")) {
                Uri uri = Uri.parse(url);
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                likeIng.setPackage("com.vkontakte.android");

                try {
                    startActivity(likeIng);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(url)));
                }

            }else if(url.equals(toWeb)){
                view.loadUrl(url);
            } else if (!url.equals(toWeb) && url.contains("profcom74.ru") && !url.contains("vk.com") && !url.contains("profcom74.ru/category/"))
            {
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
              //  view.loadUrl(url);
            }
            return true;
        }
    }
    private class DownloadHtmlSource extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... params){
            String url = params[0];
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);
            HttpResponse response = null;
            try {
                response = client.execute(request);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String html = "";
            InputStream in = null;
            try {
                in = response.getEntity().getContent();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder str = new StringBuilder();
            String line = null;
            try {
                while((line = reader.readLine()) != null)
                {
                    str.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            html = str.toString();
            new NewThread().execute(html);
            return html;
        }
    }

    public class NewThread extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... arg) {
            String page = arg[0];

            Document doc;

            doc = Jsoup.parse(page);

            Elements body = doc.body().children();

            //  String htmlBody = body.toString();
            Elements masthead = doc.select("div#left-area");
            String htmlBody = masthead.toString();
            String changedColor = htmlBody.replaceAll("color: #000000;","");
            if(htmlBody.contains("color: #334444")){
                changedColor = htmlBody.replaceAll("color: #334444;","");
            } else if (htmlBody.contains("color: #404040")){
                changedColor = htmlBody.replaceAll("color: #404040;","");
            } else if (htmlBody.contains("color: #676761")){
                changedColor = htmlBody.replaceAll("color: #676761;","");
            } else if (htmlBody.contains("color: #2B587A")){
                changedColor = htmlBody.replaceAll("color: #2B587A;","color : #31D1FF;");
            } else if  (htmlBody.contains("color: #343432")){
                changedColor = htmlBody.replaceAll("color: #343432;","");
            }

          //  Log.w("htmlBody",""+masthead);
         //  Log.w("changed",changedColor);
            publishProgress(changedColor);
            return changedColor;
        }

        @Override
        protected void onPostExecute(String result) {

        }

        protected void onSaveInstanceState(Bundle outState) {

            web.saveState(outState);
        }

        @Override
        protected void onProgressUpdate(String... val){
            super.onProgressUpdate(val);
            String htmlBody = val[0];
            web.loadDataWithBaseURL("file:///android_asset/","<html><head><style type=\"text/css\">@font-face {\n" +
                    "   font-family: 'OpenSans';\n" +
                    "   src: url('OpenSans-Regular.ttf');\n" +
                    "} p{color: rgb(255,255,255);font-family: 'OpenSans'; font-size: 20px;}  .meta-info a {color: rgba(16, 247, 255, 1);} a { color: rgba(4, 204, 237, 1);}  </style><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, target-densityDpi=device-dpi\"><style>img{max-width:100%%;height:auto !important;width:100% ;};</style></head><body><font color='white' face=\"Lucida Console\">"+htmlBody+"</font></body></html>","text/html","UTF-8",null);
        }
    }


}