package com.susudashonline;

import java.io.Serializable;

/**
 * Created by vendetta on 30.08.14.
 */
public class ContactItem implements Serializable {
    private String name;
    private String image;
    private String address;
    private String dekanat;
    private String phone;
    private String email;
    private String email_two;
    private String site;
    private String site_two;
    private String time;
    private String shedule;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDekanat() {
        return dekanat;
    }

    public void setDekanat(String dekanat) {
        this.dekanat = dekanat;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail_two() {
        return email_two;
    }

    public void setEmail_two(String email_two) {
        this.email_two = email_two;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getSite_two() {
        return site_two;
    }

    public void setSite_two(String site_two) {
        this.site_two = site_two;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getShedule() {
        return shedule;
    }

    public void setShedule(String shedule) {
        this.shedule = shedule;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getTime3() {
        return time3;
    }

    public void setTime3(String time3) {
        this.time3 = time3;
    }

    public String getTime4() {
        return time4;
    }

    public void setTime4(String time4) {
        this.time4 = time4;
    }

    public String getShedule1() {
        return shedule1;
    }

    public void setShedule1(String shedule1) {
        this.shedule1 = shedule1;
    }

    public String getShedule2() {
        return shedule2;
    }

    public void setShedule2(String shedule2) {
        this.shedule2 = shedule2;
    }

    public String getShedule3() {
        return shedule3;
    }

    public void setShedule3(String shedule3) {
        this.shedule3 = shedule3;
    }

    public String getShedule4() {
        return shedule4;
    }

    public void setShedule4(String shedule4) {
        this.shedule4 = shedule4;
    }

    private String time1;
    private String time2;
    private String time3;
    private String time4;
    private String shedule1;
    private String shedule2;
    private String shedule3;
    private String shedule4;
    boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
