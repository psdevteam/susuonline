package com.susudashonline;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by vendetta on 08.07.14.
 */
public class Settings extends ActionBarActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mNavigationNames;
    int termNumber;
    int termPart;
    int weekNumber;
    Spinner facultySpinner;
    Spinner groupSpinner;
    Switch weekSwitch;
    List<FacultyItem> facultyList;
    List<GroupItem> groupList;
    List<ClassItem> schedule;
    List<String> groupNumbers;
    List<String> facultyNames;
    List<CharSequence> weekDays;
    ArrayAdapter<String> facultyAdapter;
    ArrayAdapter<String> groupAdapter;
    Button getButton;
    List<CharSequence> allowedFaculties;
    GroupItem selectedGroup;
    FacultyItem selectedFaculty;
    DBHandler dbHandler;
    public Field mDragger;
    public Field mEdgeSize;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
      //  Log.d("onCreate","CREATED");
        //create database handler
        dbHandler = new DBHandler(this);

        //find spinners and buttons
        facultyNames = new ArrayList<String>();
        groupNumbers = new ArrayList<String>();
        weekDays = Arrays.asList(getResources().getTextArray(R.array.weekdays));
        facultyNames.add(getResources().getString(R.string.select_faculty));
        groupNumbers.add(getResources().getString(R.string.group));
        allowedFaculties = Arrays.asList(getResources().getTextArray(R.array.allowed_faculties));
        facultySpinner = (Spinner) findViewById(R.id.faculty_spinner);
        groupSpinner = (Spinner) findViewById(R.id.group_spinner);
        getButton = (Button) findViewById(R.id.schedule_button);
        weekSwitch = (Switch) findViewById(R.id.week_switch);
        mTitle = getResources().getString(R.string.title_activity_schedule);

        mDrawerTitle = getResources().getString(R.string.navigation_title);
        mNavigationNames = getResources().getStringArray(R.array.navigation_names);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);







        new AlertDialog.Builder(this)

               // .setTitle("Настройки")
                .setMessage("Желаете выбрать факультет и группу? (это удалит расписание, если оно уже загружено)")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dbHandler.delete();

                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Settings.this.finish();

                    }
                })

                .show();



        try {
            mDragger = mDrawerLayout.getClass().getDeclaredField("mLeftDragger");//mRightDragger for right obviously
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mDragger.setAccessible(true);
        ViewDragHelper draggerObj = null;
        try {
            draggerObj = (ViewDragHelper) mDragger.get(mDrawerLayout);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            mEdgeSize = draggerObj.getClass().getDeclaredField("mEdgeSize");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mEdgeSize.setAccessible(true);
        int edge = 0;
        try {
            edge = mEdgeSize.getInt(draggerObj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            mEdgeSize.setInt(draggerObj, edge * 12); //optimal value as for me, you may set any constant in dp
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }




        //create and set on click listener for button get/refresh
        View.OnClickListener scheduleButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    trimCache(Settings.this);
                } catch (Exception e) {

                    e.printStackTrace();
                }


                if (selectedGroup == null && selectedFaculty == null) {
                    Toast.makeText(Settings.this, getResources().getString(R.string.need_fandg), Toast.LENGTH_SHORT).show();
                    getButton.setEnabled(false);
                } else {

                    try {

                       // reset();
                        selectedGroup = groupList.get(groupSpinner.getSelectedItemPosition());
                        selectedFaculty = facultyList.get(facultySpinner.getSelectedItemPosition() - 1);
                        dbHandler.setCurrentScheduleParameters(selectedGroup);
                        //get termNumber, termPart and weekNumber
                        //assign defaults
                        termNumber = 1;
                        termPart = 1;

                        //set week number
                        Calendar calendar = Calendar.getInstance();
                        weekNumber = (calendar.get(Calendar.WEEK_OF_YEAR) % 2) + 1;

                        //get current term
                        GetRequestTask getTermPart = new GetRequestTask(Settings.this) {
                            @Override
                            public void onResponseReceived(String result) {
                                //parsing input string
                                try {
                                    //check for null input
//	                		Log.d("mytest", "term part response:" + result + ":");
                                    if ((result != null) && (!result.equals(""))) {

                                        //get term number
                                        int itemStart = result.indexOf("\"TermNumber\":") + "\"TermNumber\":".length();
                                        int itemEnd = result.indexOf(",", itemStart);
                                        String tn = result.substring(itemStart, itemEnd);
                                        //Log.d("parsing", "start: " + itemStart + "\nend: " + itemEnd + "\nid string: " + id);

                                        //get term part
                                        itemStart = result.indexOf("\"TermPart\":") + "\"TermPart\":".length();
                                        itemEnd = result.indexOf("}", itemStart);
                                        String tp = result.substring(itemStart, itemEnd);
                                        //Log.d("parsing", "start: " + itemStart + "\nend: " + itemEnd + "\nname string: " + name);

                                        termNumber = Integer.parseInt(tn);
                                        termPart = Integer.parseInt(tp);
                                        //Log.d("mytest", "set termNumber=" + termNumber + " termPart=" + termPart);

                                        //write term info to db
                                        dbHandler.addTermInfo(selectedGroup, termNumber, termPart);
                                    } else {

                                        //read data from db
                                        Map<String, Integer> termInfo = dbHandler.getTermInfo(selectedGroup);
                                        termNumber = termInfo.get("term_number");
                                        termPart = termInfo.get("term_part");
                                    }
                                    //Log.d("mytest", "set termNumber=" + termNumber + " termPart=" + termPart);

                                    if (weekNumber == 1)
                                        weekSwitch.setChecked(true);
                                    else
                                        weekSwitch.setChecked(false);

                                    //Log.d("exceptions", "number + part: " + termNumber + " / " + termPart);

                                } catch (Exception e) {
//                                    Log.d("exceptions", "error parsing term number " + e.toString());
                                }
                            }
                        };
//	            Log.d("mytest", getResources().getString(R.string.univeris_term_part_link) + selectedGroup.id);
                        getTermPart.execute(getResources().getString(R.string.univeris_term_part_link) + selectedGroup.id);

                        //get schedule
                        GetRequestTask getSchedule = new GetRequestTask(Settings.this) {
                            @Override
                            public void onResponseReceived(String result) {
                                UniverisParser up = new UniverisParser();

                                try {
                                    if (result != null) {
                                        //Log.d("exceptions", "result for parsing" + result);
                                        schedule = up.parseSchedule(result);

                                        //write classes to db
                                        dbHandler.addClasses(schedule, selectedGroup);
                                    } else {
                                        //read data from db
                                        schedule = dbHandler.getClasses(selectedGroup, termPart, termNumber);
                                    }
                                    //call method that fills the table (schedule, termNumber, termPart, weekNumber)
                                    if (schedule == null) {
                                        Toast.makeText(Settings.this, getResources().getString(R.string.no_server_classes), Toast.LENGTH_SHORT).show();
                                        ((ScheduleActivity) activity).fillScheduleTable(schedule, termNumber, termPart, weekNumber);
                                    }//activity deleted
                                    else {
                                        Toast.makeText(Settings.this, getResources().getString(R.string.down), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
//                                    Log.d("exceptions", "error with schedule:\n" + e.toString());
                                }
                            }
                        };
//	            Log.d("mytest", getResources().getString(R.string.univeris_schedule_link) + selectedGroup.id);
                        getSchedule.execute(getResources().getString(R.string.univeris_schedule_link) + selectedGroup.id);
                    } catch (Exception e) {
//                        Log.d("exceptions", "problem in refresh button handler" + e.toString());
                    }
                }
            }
        };



            getButton.setOnClickListener(scheduleButtonListener);



        //setting an adapter for group spinner

        groupAdapter = new ArrayAdapter<String>(this, R.layout.grouphint, groupNumbers);
        groupAdapter.setDropDownViewResource(R.layout.spinner_drop);
        groupSpinner.setAdapter(groupAdapter);
        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getBaseContext(), "selected: " + position, Toast.LENGTH_SHORT).show();

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        //setting an adapter for faculty spinner
        facultyAdapter = new ArrayAdapter<String>(this, R.layout.newsp, facultyNames);
        facultyAdapter.setDropDownViewResource(R.layout.spinner_drop);
        facultySpinner.setAdapter(facultyAdapter);
        facultySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //if something selected
                if (position != 0) {
                    selectedFaculty = facultyList.get(position - 1);
                    //Toast.makeText(getBaseContext(), selectedItem.name + position, Toast.LENGTH_SHORT).show();

                    //depending on selected faculty get a list of groups
                    GetRequestTask getGroupList = new GetRequestTask(Settings.this) {
                        @Override
                        public void onResponseReceived(String result) {
                            UniverisParser up = new UniverisParser();

                            try {
                                if (result != null) {
                                    groupList = up.parseGroups(result);

                                    //add groups to db
                                    dbHandler.addGroups(groupList, selectedFaculty);
                                } else {
                                    //read groups from db
                                    groupList = dbHandler.getGroups(selectedFaculty);
                                }

                                //fill the new data
                                groupNumbers.clear();
                                for (GroupItem group : groupList) {
                                    groupNumbers.add("" + group.groupNumber);
                                }

                                //notify adapter that it should refresh
                                groupAdapter.notifyDataSetChanged();

                                //make group spinner visible
                                groupSpinner.setVisibility(View.VISIBLE);


                            } catch (Exception e) {
                             //   Log.d("exceptions", "in schedule activity after data set changed:\n" + e.toString());
                            }
                            getButton.setEnabled(true);
                        }
                    };
//                Log.d("mytest", getResources().getString(R.string.univeris_groups_link) + selectedFaculty.id);
                    getGroupList.execute(getResources().getString(R.string.univeris_groups_link) + selectedFaculty.id);

                }


            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        //get faculty list
        GetRequestTask getFacultyList = new GetRequestTask(Settings.this) {
            @Override
            public void onResponseReceived(String result) {
                UniverisParser up = new UniverisParser();

                try {
                    if (result != null) {
//        			Log.d("mytest", "result is null " + (result == null));

                        facultyList = up.parseFaculties(result);

                        //fill the new data
                        List<FacultyItem> filteredFacultyList = new ArrayList<FacultyItem>();
                        for (FacultyItem faculty : facultyList) {
                            //filter faculties
                            if (allowedFaculties.contains(faculty.name)) {
                                filteredFacultyList.add(faculty);
                                facultyNames.add(faculty.name);
                            }
                        }
                        facultyList = filteredFacultyList;

                        //insert faculties into db
                        dbHandler.addFaculties(filteredFacultyList);
                    } else {
                        //read faculties from db
                        facultyList = dbHandler.getFaculties();

                        for (FacultyItem faculty : facultyList) {
                            facultyNames.add(faculty.name);
                        }
                    }

                    //notify adapter that it should refresh
                    facultyAdapter.notifyDataSetChanged();
                } catch (Exception e) {
//                    Log.d("exceptions", "in schedule activity after data set changed:\n" + e.toString());
                }
            }
        };
//    Log.d("mytest", getResources().getString(R.string.univeris_faculties_link));
        getFacultyList.execute(getResources().getString(R.string.univeris_faculties_link));


        // set a custom shadow that overlays the main content when the drawer opens
        // set up the drawer's list view with items and click listener


        final Typeface font = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

      ArrayAdapter<String> navAdapter = new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mNavigationNames) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                switch (position) {
                    case 0:
                        Drawable draw0 = getResources().getDrawable(R.drawable.ic_action_about);
                        DisplayMetrics metrics = getResources().getDisplayMetrics();
                        int mX = Math.round(50*metrics.density);
                        draw0.setBounds(0, 0, mX, mX);
                        TextView tw2 = (TextView) view;
                        tw2.setCompoundDrawables(draw0, null, null, null);
//                        Log.d("mytest", tw2.getText().toString());
                        tw2.setTypeface(font);
                        break;

                    case 1:
                        TextView tw1 = (TextView) view ;
//                        Log.d("mytest", tw1.getText().toString());
                        Drawable draw1 = getResources().getDrawable(R.drawable.ic_news);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw1.setBounds(0, 0, mX, mX);
                        tw1.setCompoundDrawables(draw1, null, null, null);
                        tw1.setTypeface(font);

                        break;
                    case 2:
                        TextView tw3 = (TextView) view;
//                        Log.d("mytest", tw3.getText().toString());
                        Drawable draw2 = getResources().getDrawable(R.drawable.ic_work);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw2.setBounds(0, 0, mX, mX);
                        tw3.setCompoundDrawables(draw2, null, null, null);
                        tw3.setTypeface(font);

                        break;
                    case 3:
                        TextView tw4 = (TextView) view;
//                        Log.d("mytest", tw4.getText().toString());
                        Drawable draw3 = getResources().getDrawable(R.drawable.ic_contacts);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw3.setBounds(0, 0, mX, mX);
                        tw4.setCompoundDrawables(draw3, null, null, null);
                        tw4.setTypeface(font);

                        break;
                    case 4:
                        TextView tw5 = (TextView) view;
//                        Log.d("mytest", tw5.getText().toString());
                        Drawable draw4 = getResources().getDrawable(R.drawable.ic_map);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw4.setBounds(0, 0, mX, mX);
                        tw5.setCompoundDrawables(draw4, null, null, null);
                        tw5.setTypeface(font);

                        break;
                    case 5:
                        TextView tw6 = (TextView) view;
//                        Log.d("mytest", tw6.getText().toString());
                        Drawable draw5 = getResources().getDrawable(R.drawable.ic_settings);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw5.setBounds(0, 0, mX, mX);
                        tw6.setCompoundDrawables(draw5, null, null, null);
                        tw6.setTypeface(font);

                        break;


                    default:
                        break;
                }

                return view;
            }
        };
        View header = (View) getLayoutInflater().inflate(R.layout.logo, null);


        mDrawerList.addHeaderView(header, "logo", false);
        mDrawerList.setAdapter(navAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


        // enable ActionBar app icon to behave as action to toggle nav drawer
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception ex) {
            Log.w("NULL", "icon");
        }
        getSupportActionBar().setHomeButtonEnabled(true);


        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                Settings.this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.sett);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.navigation_title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 1:
                    try{
                        Intent iShed = new Intent(Settings.this, ScheduleActivity.class);
                        Settings.this.finish();
                        startActivity(iShed);
                    } catch (Exception ex){
                     //   Log.w("Schedule","didn't open");
                    }
                break;
                case 2:
                    //start schedule
                    try {
                        Intent intentNews = new Intent(Settings.this, MainActivity.class);
                        Settings.this.finish();
                        startActivity(intentNews);
                    } catch (Exception ex) {
                        //Log.w("MainAct", "didn't open");
                    }
                    break;
                case 3:
                    //start schedule
                    Intent intentWork = new Intent(Settings.this, WorkActivity.class);
                    Settings.this.finish();
                    startActivity(intentWork);

                    break;
                case 4:
                    Intent iContact = new Intent(Settings.this, ContactsActivity.class);
                    Settings.this.finish();
                    startActivity(iContact);
                    break;
                case 5:
                    Intent intentMap = new Intent(Settings.this, ZoomActivity.class);
                    Settings.this.finish();
                    startActivity(intentMap);
                    break;
                default:
                    //default menu behavior

                    break;
            }

            // update selected item and title, then close the drawer
//	            mDrawerList.setItemChecked(position, true);
            try {
                //  setTitle(R.string.news);


                getSupportActionBar().setTitle(R.string.sett);

            } catch (Exception ex) {
               // Log.w("SecondAct", "don't open");
            }
            mDrawerLayout.closeDrawer(mDrawerList);

        }
    }





        public void setTitle(CharSequence title) {

            mTitle = title;

            }

        @Override
        protected void onPostCreate(Bundle savedInstanceState) {
            super.onPostCreate(savedInstanceState);
            // Sync the toggle state after onRestoreInstanceState has occurred.
            getSupportActionBar().setTitle(R.string.sett);
            mDrawerToggle.syncState();

        }


        @Override
        public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);
            // Pass any configuration change to the drawer toggls
            mDrawerToggle.onConfigurationChanged(newConfig);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Pass the event to ActionBarDrawerToggle, if it returns
            // true, then it has handled the app icon touch event
            if (mDrawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
            // Handle your other action bar items...

            switch (item.getItemId()) {
                case R.id.devInfoMenu:

// Linkify the message
                    final SpannableString s = new SpannableString(getResources().getString(R.string.dialog_message));
                    Linkify.addLinks(s, Linkify.ALL);

                    final AlertDialog d = new AlertDialog.Builder(Settings.this)
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setMessage( s )
                            .create();

                    d.show();

                    // Make the textview clickable. Must be called after show()
                    ((TextView)d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

                    break;
                default:
                    //default menu behavior

                    break;
            }
            return super.onOptionsItemSelected(item);
        }

    public void refresh(final MenuItem item) {
     /* Attach a rotating ImageView to the refresh item as an ActionView */
        LayoutInflater inflater = (LayoutInflater)getApplication().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView) inflater.inflate(R.layout.clear_layout, null);





    }

    public void completeRefresh(final MenuItem item) {
        if (item != null && item.getActionView() != null) {
            item.getActionView().clearAnimation();
            item.setActionView(null);
        }
    }



    public void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
              //  Toast.makeText(this,"Кэш очищен!",Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.devinfo_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    }



