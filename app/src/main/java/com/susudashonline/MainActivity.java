package com.susudashonline;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.susudashonline.customviews.PullToRefreshListView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class MainActivity extends ActionBarActivity {
	MainActivity thisActivity = this;
	private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mNavigationNames;

	boolean isRefresh = true;
    ProgressDialog dialog;
    //Button loadMoreButton;
    LayoutInflater ltInflater;
    ArrayAdapter<String> navAdapter;
    public Integer[] Titles = {
            R.string.news,
            R.string.title_activity_schedule
    };
    public Field mDragger;
    public Field mEdgeSize;


    public Elements titles;
    public Elements picture;


    private ArrayList<FeedItem> feedList = null;
    private com.susudashonline.customviews.PullToRefreshListView feedListView = null;
    public CustomListAdapter adapter;

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    static final String SERVER_URL = "http://infosusu.esy.es/pushserver/register.php";

    String SENDER_ID = "144726057749";

    static final String TAG = "GCMDemo";
    public int currentPage;

    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;

    String regid;
    Context context;




    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }



    @Override
    protected void onRestart(){
        super.onRestart();

    }


        @Override
	protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            currentPage = 1;
            context = getApplicationContext();
            new SimpleEula(this).show();

            getSupportActionBar().setTitle(Titles[0]);




//            Intent iService = getIntent();
//            startService(new Intent(this, NotifService.class));

            feedListView = (com.susudashonline.customviews.PullToRefreshListView) findViewById(R.id.listView1);

            feedList = new ArrayList<FeedItem>();

           if (isOnline()) {
               new NewThread().execute();

               updateList();


               feedListView
                       .setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
                           @Override
                           public void onRefresh() {
                               // Do work to refresh the list here.
                               new GetDataTask().execute();
                           }
                       });

           }
            //add footer view
            ltInflater = getLayoutInflater();


            if (checkPlayServices()) {
                gcm = GoogleCloudMessaging.getInstance(this);
                regid = getRegistrationId(context);
               // Log.w("regId",regid);
                if (regid.isEmpty()) {
                    registerInBackground();
                    Log.w("Register in push service","process");
                } else {
                    Log.i(TAG, "No valid Google Play Services APK found.");

                }
            }

	    mDrawerTitle = getResources().getString(R.string.navigation_title);
        mNavigationNames = getResources().getStringArray(R.array.navigation_names);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

            try {
                 mDragger = mDrawerLayout.getClass().getDeclaredField("mLeftDragger");//mRightDragger for right obviously
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            mDragger.setAccessible(true);
            ViewDragHelper draggerObj = null;
            try {
                draggerObj = (ViewDragHelper) mDragger.get(mDrawerLayout);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            try {
                mEdgeSize = draggerObj.getClass().getDeclaredField("mEdgeSize");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            mEdgeSize.setAccessible(true);
            int edge = 0;
            try {
                edge = mEdgeSize.getInt(draggerObj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            try {
                mEdgeSize.setInt(draggerObj, edge * 12); //optimal value as for me, you may set any constant in dp
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            // set a custom shadow that overlays the main content when the drawer opens
        // set up the drawer's list view with items and click listener
        final Typeface font = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

	    navAdapter = new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mNavigationNames) {
	    	@Override
	    	public View getView(int position, View convertView, ViewGroup parent) {  
	    		View view = super.getView(position, convertView, parent);  
	    		
		    		switch (position) {
		    		case 0:
		        		Drawable draw0 = getResources().getDrawable(R.drawable.ic_action_about);
		        		DisplayMetrics metrics = getResources().getDisplayMetrics();
		        		int mX = Math.round(50*metrics.density);
		        		draw0.setBounds(0, 0, mX, mX);
		        		TextView tw2 = (TextView) view;
		        		tw2.setCompoundDrawables(draw0, null, null, null);
		        	//	Log.d("mytest", tw2.getText().toString());
                        tw2.setTypeface(font);

		        		break;
		        		
		        	case 1:
		        		TextView tw1 = (TextView) view ;
		        	//	Log.d("mytest", tw1.getText().toString());
		        		Drawable draw1 = getResources().getDrawable(R.drawable.ic_news);
		        		metrics = getResources().getDisplayMetrics();
		        		mX = Math.round(50*metrics.density);
		        		draw1.setBounds(0, 0, mX, mX);
		        		tw1.setCompoundDrawables(draw1, null, null, null);
                        tw1.setTypeface(font);
		        		
		        		break;
                        case 2:
                            TextView tw3 = (TextView) view;
                      //      Log.d("mytest", tw3.getText().toString());
                            Drawable draw2 = getResources().getDrawable(R.drawable.ic_work);
                            metrics = getResources().getDisplayMetrics();
                            mX = Math.round(50*metrics.density);
                            draw2.setBounds(0, 0, mX, mX);
                            tw3.setCompoundDrawables(draw2, null, null, null);
                            tw3.setTypeface(font);

                            break;
                        case 3:
                            TextView tw4 = (TextView) view;
                         //   Log.d("mytest", tw4.getText().toString());
                            Drawable draw3 = getResources().getDrawable(R.drawable.ic_contacts);
                            metrics = getResources().getDisplayMetrics();
                            mX = Math.round(50*metrics.density);
                            draw3.setBounds(0, 0, mX, mX);
                            tw4.setCompoundDrawables(draw3, null, null, null);
                            tw4.setTypeface(font);

                            break;
                        case 4:
                            TextView tw5 = (TextView) view;
                       //     Log.d("mytest", tw5.getText().toString());
                            Drawable draw4 = getResources().getDrawable(R.drawable.ic_map);
                            metrics = getResources().getDisplayMetrics();
                            mX = Math.round(50*metrics.density);
                            draw4.setBounds(0, 0, mX, mX);
                            tw5.setCompoundDrawables(draw4, null, null, null);
                            tw5.setTypeface(font);

                            break;
                        case 5:
                            TextView tw6 = (TextView) view;
                         //   Log.d("mytest", tw6.getText().toString());
                            Drawable draw5 = getResources().getDrawable(R.drawable.ic_settings);
                            metrics = getResources().getDisplayMetrics();
                            mX = Math.round(50*metrics.density);
                            draw5.setBounds(0, 0, mX, mX);
                            tw6.setCompoundDrawables(draw5, null, null, null);
                            tw6.setTypeface(font);

                            break;


		    		default:
		    			break;
		        	}
		    		
		    		return view;  
	    		} 
	    };
        View header = (View)getLayoutInflater().inflate(R.layout.logo,null);


        mDrawerList.addHeaderView(header,"logo",false);
        mDrawerList.setAdapter(navAdapter);
      //  mDrawerList.setItemChecked(position,true);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
            	super.onDrawerClosed(view);
                getSupportActionBar().setTitle(Titles[0]);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
            	super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.navigation_title);
               invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);


	}

    public void updateList(){
        feedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = feedListView.getItemAtPosition(position);
                FeedItem newsData = (FeedItem) o;
                try {
                    Intent intent = new Intent(MainActivity.this, NewsDetailsActivity.class);
                    intent.putExtra("feed", newsData);
                    startActivity(intent);

                } catch (Exception e) {
                    Intent intent = new Intent(MainActivity.this, NewsDetailsActivity.class);
                    intent.putExtra("feed", newsData);
                    startActivity(intent);
                }
            }
        });
    }

    public void addItemsToList(){
        adapter.notifyDataSetChanged();
        feedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = feedListView.getItemAtPosition(position);
                FeedItem newsData = (FeedItem) o;
                try {
                    Intent intent = new Intent(MainActivity.this, NewsDetailsActivity.class);
                    intent.putExtra("feed", newsData);
                    startActivity(intent);

                } catch (Exception e) {
                    Intent intent = new Intent(MainActivity.this, NewsDetailsActivity.class);
                    intent.putExtra("feed", newsData);
                    startActivity(intent);
                }
            }
        });
    }
    private class GetDataTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            feedList.clear();

        }
     /*   @Override
        protected void onProgressUpdate(Integer... values){
           // super.onProgressUpdate();

        } */

        @Override
        protected String doInBackground(String... arg) {


            Document doc;
            try {

                doc = Jsoup.connect("http://profcom74.ru/news/page/1/").get();

                titles = doc.select("h2.et_pt_title");
                Elements date = doc.select("p.et_pt_blogmeta");
                Elements imgContainingDiv = doc.select("div[class=et_pt_thumb2 alignleft]");

                Elements picture = imgContainingDiv.select("img");
                Element pic = null;
                Element singDate = null;
                String miniDate  = null;
                Elements linkPage = titles.select("a");
                Element lp = null;
                String link = null;
                int i = 0;

                for (Element title : titles) {
                    FeedItem item = new FeedItem();
                    item.setTitle(title.text());

                //    Log.w("title", "" + item.getTitle());
                    if(i<10) {
                        pic = picture.get(i);
                        singDate = date.get(i);
                        miniDate = singDate.text();
                        item.setDate(miniDate);
                        lp = linkPage.get(i);
                        link = lp.attr("href");
                        item.setUrl(link);
                        i++;
                     //   Log.w("i",""+i);

                    }
               //     Log.w("link",""+link);
               //     Log.w("date",""+miniDate);

                    String iurl = pic.attr("src");
                    if (iurl.contains("http://profcom74.ru/wp-content/uploads/2014/")) {
                        item.setAttachmentUrl(iurl);
                    }

             //       Log.w("imgUrl", "" + iurl);



                    feedList.add(item);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            adapter = new CustomListAdapter(MainActivity.this,feedList);
            feedListView.setAdapter(adapter);
            // Call onRefreshComplete when the list has been refreshed.
            feedListView.onRefreshComplete();
            currentPage=2;
            super.onPostExecute(result);
        }
    }

        private boolean checkPlayServices() {
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.SUCCESS) {
                if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                            PLAY_SERVICES_RESOLUTION_REQUEST).show();
                } else {
                    Log.w(TAG, "This device is not supported.");
                    finish();
                }
                return false;
            }
            return true;
        }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.w(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.w(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(WorkActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void registerInBackground() {
        new AsyncTask<Void,Void,String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                    Map<String, String> paramsMap = new HashMap<String, String>();
                    paramsMap.put("regId", regid);

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    sendRegistrationIdToBackend(SERVER_URL,paramsMap);

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {

            }
        }.execute(null, null, null);

    }



    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }



    private void sendRegistrationIdToBackend(String endpoint, Map<String, String> params)throws IOException {
        // Your implementation here.

        URL url;
        try {
            url = new URL(endpoint);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext()) {
            Map.Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=')
                    .append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }
        String body = bodyBuilder.toString();
        Log.w(TAG, "Posting '" + body + "' to " + url);
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = null;
        try {
            Log.w("URL", "> " + url);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
                throw new IOException("Post failed with error code " + status);
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public class NewThread extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... arg) {

            Document doc;
            try {

                doc = Jsoup.connect("http://profcom74.ru/news/page/1/").get();

                titles = doc.select("h2.et_pt_title");
                Elements date = doc.select("p.et_pt_blogmeta");
                Elements imgContainingDiv = doc.select("div[class=et_pt_thumb2 alignleft]");

                Elements picture = imgContainingDiv.select("img");
                Element pic = null;
                Element singDate = null;
                String miniDate  = null;
                Elements linkPage = titles.select("a");
                Element lp = null;
                String link = null;
                int i = 0;
                for (Element title : titles) {
                    FeedItem item = new FeedItem();
                    item.setTitle(title.text());

               //     Log.w("title", "" + item.getTitle());
                    if(i<10) {
                        pic = picture.get(i);
                        singDate = date.get(i);
                        miniDate = singDate.text();
                        item.setDate(miniDate);
                        lp = linkPage.get(i);
                        link = lp.attr("href");
                        item.setUrl(link);
                        i++;
                    }
               //     Log.w("link",""+link);
                //    Log.w("date",""+miniDate);

                    String iurl = pic.attr("src");
                    if (iurl.contains("http://profcom74.ru/wp-content/uploads/2014/")) {
                        item.setAttachmentUrl(iurl);
                    }

                //    Log.w("imgUrl", "" + iurl);

                    feedList.add(item);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            adapter = new CustomListAdapter(MainActivity.this,feedList);
            feedListView.setAdapter(adapter);

            feedListView.setOnScrollListener(new EndlessScrollListener());
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else Toast.makeText(MainActivity.this,R.string.need_inet,Toast.LENGTH_SHORT).show();
        return false;
    }
	

    private class EndlessScrollListener implements AbsListView.OnScrollListener {

        private int visibleThreshold = 4;

        private int previousTotal = 0;
        private boolean loading = true;

        public EndlessScrollListener() {
        }
        public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {


            if (!loading  && ((firstVisibleItem+visibleItemCount)>=totalItemCount) ) {

                String url = getResources().getString(R.string.newsUrl);
                new DownloadMoreFeed().execute(url+currentPage+"/");
             //   currentPage++;
                Log.w("loading page", "" + currentPage);
                loading = true;

            } else if (!loading && ((firstVisibleItem+visibleItemCount)<totalItemCount)){
                //nothing
            }


            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                    Log.w("Current page",""+currentPage);
                }
            }



        }

        private class DownloadMoreFeed extends AsyncTask<String, Integer, Void> {

            @Override
            protected void onProgressUpdate(Integer... values) {
            }

            @Override
            protected void onPostExecute(Void result) {
                if (null != feedList) {
                   addItemsToList();

                }

                feedListView.setOnScrollListener(new EndlessScrollListener());

            }

            @Override
            protected Void doInBackground(String... params) {
                String url = params[0];
                Document doc;
                try {

                    doc = Jsoup.connect(url).get();

                    titles = doc.select("h2.et_pt_title");
                    Elements date = doc.select("p.et_pt_blogmeta");
                    Elements imgContainingDiv = doc.select("div[class=et_pt_thumb2 alignleft]");

                    Elements picture = imgContainingDiv.select("img");
                    Element pic = null;
                    Element singDate = null;
                    String miniDate  = null;
                    Elements linkPage = titles.select("a");
                    Element lp = null;
                    String link = null;
                    int i = 0;
                    for (Element title : titles) {
                        FeedItem item = new FeedItem();
                        item.setTitle(title.text());

                    //    Log.w("title", "" + item.getTitle());
                        if(i<10) {
                            pic = picture.get(i);
                            singDate = date.get(i);
                            miniDate = singDate.text();
                            item.setDate(miniDate);
                            lp = linkPage.get(i);
                            link = lp.attr("href");
                            item.setUrl(link);
                            i++;
                        }
                    //    Log.w("link",""+link);
                     //   Log.w("date",""+miniDate);

                        String iurl = pic.attr("src");
                        if (iurl.contains("http://profcom74.ru/wp-content/uploads/2014/")) {
                            item.setAttachmentUrl(iurl);
                        }

                    //    Log.w("imgUrl", "" + iurl);



                        feedList.add(item);

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;


            }
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }
    }




	//new -----------------------------------------------------------
	/* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        	switch (position) {
        	case 1:
        		//start schedule
        		Intent intent = new Intent(thisActivity, ScheduleActivity.class);
        		startActivity(intent);

        		break;
                case 3:
                    //start schedule
                    Intent intentWork = new Intent(thisActivity, WorkActivity.class);
                    startActivity(intentWork);

                    break;
                case 4:
                    Intent iContacts = new Intent(thisActivity, ContactsActivity.class);
                    startActivity(iContacts);
                    break;
                case 5:
                    Intent intentMap = new Intent(thisActivity, ZoomActivity.class);
                    startActivity(intentMap);
                break;
                case 6:
                    Intent intentSet = new Intent(thisActivity, Settings.class);
                    startActivity(intentSet);
                break;
    		default:
    			//default menu behavior
    			
    			break;
        	}
        	// update selected item and title, then close the drawer
//            mDrawerList.setItemChecked(position, true);



            getSupportActionBar().setTitle(Titles[0]);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    @Override
    public void setTitle(CharSequence title) {

        mTitle = title;

    }
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        getSupportActionBar().setTitle(Titles[0]);
       // getSupportActionBar().setIcon(R.drawable.abicon);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)

               // .setTitle(saving)
                .setMessage("Выйти из ЮУрГУ Онлайн?")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String markedApp = "marked!";
                        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                        boolean hasBeenShown = prefs.getBoolean(markedApp, false);
                        if(!hasBeenShown){ //if(hasBeenShown == false) was here



                            //Includes the updates as well so users know what changed.
                            String message = "Оцените пожалуйста приложение, если оно Вам понравилось/не понравилось и если вы еще этого не сделали :)";

                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)

                                    .setMessage(message)
                                    .setPositiveButton("Оценить", new Dialog.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            // Mark this version as read.
                                            Intent intent = new Intent(Intent.ACTION_VIEW);
                                            intent.setData(Uri.parse("market://details?id=com.susudashonline"));
                                            startActivity(intent);
                                            SharedPreferences.Editor editor = prefs.edit();
                                            editor.putBoolean(markedApp, true);
                                            editor.apply(); // .commit() was here
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .setNegativeButton("Выйти", new Dialog.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Close the activity as they have declined the EULA
                                            MainActivity.this.finish();
                                        }

                                    });
                            builder.create().show();
                        } else {

                            MainActivity.this.finish();
                        }

                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })
              //  .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }
}


