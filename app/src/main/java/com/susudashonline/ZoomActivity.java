package com.susudashonline;

import android.animation.Animator;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;


/**
 * Created by vendetta on 09.07.14.
 */
public class ZoomActivity extends ActionBarActivity {
    static final String PHOTO_TAP_TOAST_STRING = "Photo Tap! X: %.2f %% Y:%.2f %% ID: %d";
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private Toast mCurrentToast;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mNavigationNames;
    // Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;
    public Field mDragger;
    public Field mEdgeSize;
    PhotoView mapImage;
    PhotoViewAttacher mAttacher;


    ArrayAdapter<String> navAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_layout);

        mNavigationNames = getResources().getStringArray(R.array.navigation_names);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);


        mapImage = (PhotoView)findViewById(R.id.mapView);
      //  mapImage.setMaxZoom(4f);

        // Set the Drawable displayed
      Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.kartapng);
    //  mapImage.setImageBitmap(bitmap);

        int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
        mapImage.setImageBitmap(scaled);

        // Attach a PhotoViewAttacher, which takes care of all of the zooming functionality.
        mAttacher = new PhotoViewAttacher(mapImage);
      // mapImage.setImageDrawable(getResources().getDrawable(R.drawable.kartapng));

        //mAttacher.update();


        // Lets attach some listeners, not required though!
        //mAttacher.setOnMatrixChangeListener(new MatrixChangeListener());
       // mAttacher.setOnPhotoTapListener(new PhotoTapListener());

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        // set a custom shadow that overlays the main content when the drawer opens
        // set up the drawer's list view with items and click listener
        final Typeface font = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

       navAdapter = new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mNavigationNames) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                switch (position) {
                    case 0:
                        Drawable draw0 = getResources().getDrawable(R.drawable.ic_action_about);
                        DisplayMetrics metrics = getResources().getDisplayMetrics();
                        int mX = Math.round(50*metrics.density);
                        draw0.setBounds(0, 0, mX, mX);
                        TextView tw2 = (TextView) view;
                        tw2.setCompoundDrawables(draw0, null, null, null);
//                        Log.d("mytest", tw2.getText().toString());
                        tw2.setTypeface(font);
                        break;

                    case 1:
                        TextView tw1 = (TextView) view ;
//                        Log.d("mytest", tw1.getText().toString());
                        Drawable draw1 = getResources().getDrawable(R.drawable.ic_news);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw1.setBounds(0, 0, mX, mX);
                        tw1.setCompoundDrawables(draw1, null, null, null);
                        tw1.setTypeface(font);

                        break;
                    case 2:
                        TextView tw3 = (TextView) view;
//                        Log.d("mytest", tw3.getText().toString());
                        Drawable draw2 = getResources().getDrawable(R.drawable.ic_work);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw2.setBounds(0, 0, mX, mX);
                        tw3.setCompoundDrawables(draw2, null, null, null);
                        tw3.setTypeface(font);

                        break;
                    case 3:
                        TextView tw4 = (TextView) view;
//                        Log.d("mytest", tw4.getText().toString());
                        Drawable draw3 = getResources().getDrawable(R.drawable.ic_contacts);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw3.setBounds(0, 0, mX, mX);
                        tw4.setCompoundDrawables(draw3, null, null, null);
                        tw4.setTypeface(font);

                        break;
                    case 4:
                        TextView tw5 = (TextView) view;
//                        Log.d("mytest", tw5.getText().toString());
                        Drawable draw4 = getResources().getDrawable(R.drawable.ic_map);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw4.setBounds(0, 0, mX, mX);
                        tw5.setCompoundDrawables(draw4, null, null, null);
                        tw5.setTypeface(font);

                        break;
                    case 5:
                        TextView tw6 = (TextView) view;
//                        Log.d("mytest", tw6.getText().toString());
                        Drawable draw5 = getResources().getDrawable(R.drawable.ic_settings);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw5.setBounds(0, 0, mX, mX);
                        tw6.setCompoundDrawables(draw5, null, null, null);
                        tw6.setTypeface(font);

                        break;


                    default:
                        break;
                }

                return view;
            }
        };




      View header = (View)getLayoutInflater().inflate(R.layout.logo,null);

        mDrawerList.addHeaderView(header, "logo", false);
        mDrawerList.setAdapter(navAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


        // enable ActionBar app icon to behave as action to toggle nav drawer
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception ex) {
            Log.w("NULL", "icon");
        }
        getSupportActionBar().setHomeButtonEnabled(true);


        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                ZoomActivity.this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.map);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.navigation_title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 1:
                    try{
                        Intent iShed = new Intent(ZoomActivity.this, ScheduleActivity.class);
                        ZoomActivity.this.finish();
                        startActivity(iShed);
                    } catch (Exception ex){
                        Log.w("Schedule","didn't open");
                    }
                    break;
                case 2:
                    //start schedule
                    try {
                        Intent intentNews = new Intent(ZoomActivity.this, MainActivity.class);
                        ZoomActivity.this.finish();
                        startActivity(intentNews);
                    } catch (Exception ex) {
                        Log.w("MainAct", "didn't open");
                    }
                    break;
                case 3:
                    //start schedule
                    Intent intentWork = new Intent(ZoomActivity.this, WorkActivity.class);
                    ZoomActivity.this.finish();
                    startActivity(intentWork);

                    break;
                case 4:
                    Intent iContact = new Intent(ZoomActivity.this, ContactsActivity.class);
                    ZoomActivity.this.finish();
                    startActivity(iContact);
                    break;
                case 6:
                    Intent intentSet = new Intent(ZoomActivity.this, Settings.class);
                    ZoomActivity.this.finish();
                    startActivity(intentSet);
                    break;
                default:
                    //default menu behavior

                    break;
            }

            // update selected item and title, then close the drawer
	            mDrawerList.setItemChecked(position, true);
            try {
                //  setTitle(R.string.news);


                getSupportActionBar().setTitle(R.string.map);

            } catch (Exception ex) {
                Log.w("SecondAct", "don't open");
            }
            mDrawerLayout.closeDrawer(mDrawerList);

        }
    }

    public void setTitle(CharSequence title) {

        mTitle = title;

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
// Need to call clean-up
       mAttacher.cleanup();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        getSupportActionBar().setTitle(R.string.map);
        mDrawerToggle.syncState();

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    private class PhotoTapListener implements PhotoViewAttacher.OnPhotoTapListener {
        @Override
        public void onPhotoTap(View view, float x, float y) {
            float xPercentage = x * 100f;
            float yPercentage = y * 100f;
            showToast(String.format(PHOTO_TAP_TOAST_STRING, xPercentage, yPercentage, view == null ? 0 : view.getId()));
        }
    }
    private void showToast(CharSequence text) {
        if (null != mCurrentToast) {
            mCurrentToast.cancel();
        }
        mCurrentToast = Toast.makeText(ZoomActivity.this, text, Toast.LENGTH_SHORT);
        mCurrentToast.show();
    }
    private class MatrixChangeListener implements PhotoViewAttacher.OnMatrixChangedListener {
        @Override
        public void onMatrixChanged(RectF rect) {
       //  Log.w("onMatrixChanged",rect.toString());
        }
    }
}



