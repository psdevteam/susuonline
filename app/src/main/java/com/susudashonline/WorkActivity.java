package com.susudashonline;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WorkActivity extends ActionBarActivity {
    WorkActivity thisActivity = this;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    GettingData ms;
    GetWallPostsTask newPosts;
    MyViewBinder vBinder;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mNavigationNames;

    final String ATTRIBUTE_NAME_TEXT = "text";
    final String ATTRIBUTE_NAME_IMAGE = "image";
    final String ATTRIBUTE_NAME_DATE = "date";
    ArrayList<Map<String, Object>> listViewContent;
    ListView lvSimple;
    SimpleAdapter sAdapter;
    int postCount;
    final int postCountStep = 20;
    final int postLimit = 101;
    DBHandler dbHandler;
    int PostNumber = 0;
    boolean isRefresh = true;
    ProgressDialog dialog;
    Button loadMoreButton;
    LayoutInflater ltInflater;
    ArrayAdapter<String> navAdapter;
    public Field mDragger;
    public Field mEdgeSize;




    private  class GettingData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(WorkActivity.this);
            dialog.setTitle("Подождите");
            dialog.setMessage("Загрузка списка новостей");
            dialog.setCancelable(false);
            dialog.setInverseBackgroundForced(false);
         //   Log.d("LOG:", "Dialog is showing");
            dialog.show();
            sAdapter.setViewBinder(new MyViewBinder());
            lvSimple.setHeaderDividersEnabled(false);
            lvSimple.setFooterDividersEnabled(false);
            lvSimple.setDividerHeight(0);
            lvSimple.addFooterView(loadMoreButton);

        }

        @Override
        protected Void doInBackground(Void... params){
//            Log.d("ATTENT!","Getting vk posts!");
            if(sAdapter!=null){
                try {
                    getVkPosts();
                    Thread.sleep(800);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
         }
            return null;



        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (isOnline()) {
                lvSimple.setAdapter(sAdapter);
            } else Toast.makeText(WorkActivity.this,R.string.need_inet,Toast.LENGTH_SHORT).show();

            mDrawerList.setAdapter(navAdapter);
            if(postCount==20) {
                dialog.dismiss();
            }
            getSupportActionBar().setTitle(R.string.work);
//            Log.d("Log","Dialog dies");







        }
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        if(isOnline()) {
            lvSimple.setAdapter(sAdapter);
        } else Toast.makeText(WorkActivity.this,R.string.need_inet,Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.work_layout);
        ms = new GettingData();

        getSupportActionBar().setTitle(R.string.work);

        dbHandler = new DBHandler(this);
        //Log.d(getString(R.string.logtag), "WorkActivity: i wanna love you");	    

        //filling the listview
        listViewContent = new ArrayList<Map<String, Object>>(/*postLimit + 20*/);
/*	    Map<String, Object> m;
	    for (int i = 0; i < texts.length; i++) {
	    	m = new HashMap<String, Object>();
	    	m.put(ATTRIBUTE_NAME_TEXT, texts[i]);
	    	m.put(ATTRIBUTE_NAME_IMAGE, imgurl);
	    	listViewContent.add(m);
	    }
*/
        String[] from = {ATTRIBUTE_NAME_TEXT, ATTRIBUTE_NAME_IMAGE, ATTRIBUTE_NAME_DATE};
        int[] to = {R.id.wtvText, R.id.wivImg, R.id.wtvDate};

        sAdapter = new SimpleAdapter(this, listViewContent, R.layout.work_item, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                if ((position % 2) == 0) {
                    view.setBackgroundColor(Color.parseColor("#00E9E9E9"));
                } else {
                    view.setBackgroundColor(Color.parseColor("#00F5F5F5"));
                }
                return view;
            }
        };
        //   sAdapter.setViewBinder(new MyViewBinder());

        lvSimple = (ListView) findViewById(R.id.work_listview);

        //add footer view
        ltInflater = getLayoutInflater();
        loadMoreButton = (Button) ltInflater.inflate(R.layout.load_more_button, null, false);
//    	Button refreshButton = (Button) ltInflater.inflate(R.layout.refresh_news_button, null, false);

        //set list view parameters
        //	lvSimple.addFooterView(loadMoreButton);
//    	lvSimple.addHeaderView(refreshButton);
        //lvSimple.setHeaderDividersEnabled(false);
        //	lvSimple.setFooterDividersEnabled(false);
        //	lvSimple.setDividerHeight(0);

        //set on click listener for load more button
        OnClickListener loadMoreButtonListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (postCount + postCountStep < postLimit) {
                    //Log.d("mytest", ""+postCount);
                    //Log.d("mytest", ""+(postCount+postCountStep));
                    GetWallPosts getWallPostsTask = new GetWallPostsTask(listViewContent, sAdapter);
                    getWallPostsTask.execute(getResources().getString(R.string.work_group_id), Integer.toString(postCount), Integer.toString(postCountStep));
                    postCount += postCountStep;
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.news_limit_reached), Toast.LENGTH_SHORT).show();
                }
            }
        };
        loadMoreButton.setOnClickListener(loadMoreButtonListener);
        ms.execute();
        //set on click listener for refresh button
//    	OnClickListener refreshButtonListener = new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				//refresh behavior
//				isRefresh = true;
//				listViewContent.clear();
//				getVkPosts();
//			}
//    	};
//    	refreshButton.setOnClickListener(refreshButtonListener);

        //set adapter for list view
        // lvSimple.setAdapter(sAdapter);

        //run query to vk (id, from, to)

        //   getVkPosts();



        //-------------------------------------------------------------------------------
        mDrawerTitle = getResources().getString(R.string.navigation_title);
        mNavigationNames = getResources().getStringArray(R.array.navigation_names);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        try {
            mDragger = mDrawerLayout.getClass().getDeclaredField("mLeftDragger");//mRightDragger for right obviously
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mDragger.setAccessible(true);
        ViewDragHelper draggerObj = null;
        try {
            draggerObj = (ViewDragHelper) mDragger.get(mDrawerLayout);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            mEdgeSize = draggerObj.getClass().getDeclaredField("mEdgeSize");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mEdgeSize.setAccessible(true);
        int edge = 0;
        try {
            edge = mEdgeSize.getInt(draggerObj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            mEdgeSize.setInt(draggerObj, edge * 12); //optimal value as for me, you may set any constant in dp
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // set a custom shadow that overlays the main content when the drawer opens
        // set up the drawer's list view with items and click listener
        final Typeface font = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        navAdapter = new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mNavigationNames) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                switch (position) {
                    case 0:
                        Drawable draw0 = getResources().getDrawable(R.drawable.ic_action_about);
                        DisplayMetrics metrics = getResources().getDisplayMetrics();
                        int mX = Math.round(50*metrics.density);
                        draw0.setBounds(0, 0, mX, mX);
                        TextView tw2 = (TextView) view;
                        tw2.setCompoundDrawables(draw0, null, null, null);
//                        Log.d("mytest", tw2.getText().toString());
                        tw2.setTypeface(font);
                        break;

                    case 1:
                        TextView tw1 = (TextView) view ;
//                        Log.d("mytest", tw1.getText().toString());
                        Drawable draw1 = getResources().getDrawable(R.drawable.ic_news);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw1.setBounds(0, 0, mX, mX);
                        tw1.setCompoundDrawables(draw1, null, null, null);
                        tw1.setTypeface(font);

                        break;
                    case 2:
                        TextView tw3 = (TextView) view;
//                        Log.d("mytest", tw3.getText().toString());
                        Drawable draw2 = getResources().getDrawable(R.drawable.ic_work);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw2.setBounds(0, 0, mX, mX);
                        tw3.setCompoundDrawables(draw2, null, null, null);
                        tw3.setTypeface(font);

                        break;
                    case 3:
                        TextView tw4 = (TextView) view;
//                        Log.d("mytest", tw4.getText().toString());
                        Drawable draw3 = getResources().getDrawable(R.drawable.ic_contacts);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw3.setBounds(0, 0, mX, mX);
                        tw4.setCompoundDrawables(draw3, null, null, null);
                        tw4.setTypeface(font);

                        break;
                    case 4:
                        TextView tw5 = (TextView) view;
//                        Log.d("mytest", tw5.getText().toString());
                        Drawable draw4 = getResources().getDrawable(R.drawable.ic_map);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw4.setBounds(0, 0, mX, mX);
                        tw5.setCompoundDrawables(draw4, null, null, null);
                        tw5.setTypeface(font);

                        break;
                    case 5:
                        TextView tw6 = (TextView) view;
//                        Log.d("mytest", tw6.getText().toString());
                        Drawable draw5 = getResources().getDrawable(R.drawable.ic_settings);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw5.setBounds(0, 0, mX, mX);
                        tw6.setCompoundDrawables(draw5, null, null, null);
                        tw6.setTypeface(font);

                        break;


                    default:
                        break;
                }

                return view;
            }
        };
        View header = (View)getLayoutInflater().inflate(R.layout.logo,null);


        mDrawerList.addHeaderView(header,"logo",false);
        //  mDrawerList.setAdapter(navAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        /*TextView lDraw = (TextView)findViewById(R.id.leftDrawItem);
        Typeface font = Typeface.createFromAsset(getAssets(), "OpenSans-Bold.ttf");
        try {
            lDraw.setTypeface(font);
        } catch (Exception e){
            Log.w("WARN!","shit");
        } */


        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.work);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.navigation_title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

//        if (savedInstanceState == null) {
//            selectItem(0);
//        }
        //-------------------------------------------------------------------------------------------------
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else Toast.makeText(WorkActivity.this,R.string.need_inet,Toast.LENGTH_SHORT).show();
        return false;
    }

    public void getVkPosts() {
        GetWallPosts getWallPostsTask = new GetWallPostsTask(listViewContent, sAdapter);
        getWallPostsTask.execute(getResources().getString(R.string.work_group_id), Integer.toString(0), Integer.toString(postCountStep));
        postCount = postCountStep;
    }

    public void refresh(final MenuItem item) {
     /* Attach a rotating ImageView to the refresh item as an ActionView */
        LayoutInflater inflater = (LayoutInflater)getApplication().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView) inflater.inflate(R.layout.refresh_layout, null);

        Animation rotation = AnimationUtils.loadAnimation(getApplication(), R.anim.refresh_rotate);
        rotation.setRepeatCount(Animation.INFINITE);
        iv.startAnimation(rotation);

        item.setActionView(iv);


    }

    public void completeRefresh(final MenuItem item) {
        if (item != null && item.getActionView() != null) {
            item.getActionView().clearAnimation();
            item.setActionView(null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.general_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.refresh_news:
                class Refresh extends AsyncTask<Void, Void, Void>{
                    @Override
                    protected void onPreExecute(){
                        super.onPreExecute();
                        refresh(item);
                        isRefresh = true;
                        listViewContent.clear();
                    }

                    @Override
                    protected Void doInBackground(Void... params){
                        try {
                            getVkPosts();
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result){
                        super.onPostExecute(result);
                        completeRefresh(item);
                    }
                }
                Refresh refButton = new Refresh();
                refButton.execute();

                break;
            default:
                //default menu behavior

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class MyViewBinder implements SimpleAdapter.ViewBinder {

        @Override
        public boolean setViewValue(View view, Object data, String textRepresentation) {
            //Log.d(getString(R.string.logtag), "in set view " + textRepresentation);
            try {
                switch (view.getId()) {
                    case R.id.wtvText:
                        if (data != null) {
                            TextView tv = (TextView) view;
                            //Log.d(getString(R.string.logtag), "setting textdata to " + data.toString());
                            Typeface font = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
                            tv.setTypeface(font);
                            tv.setText(data.toString());
                        }

                        return true;
                    case R.id.wivImg:
                        //if (data != null) {
                        ImageView iv = (ImageView) view;
                        //new DownloadImageTask(iv).execute(textRepresentation);
                        Bitmap pic = ((PictureItem) data).picture;
                        //if (pic != null)
                        iv.setImageBitmap(pic);
                        //}

                        return true;
                    case R.id.wtvDate:
                        TextView tv = (TextView) view;
                        Date date = (Date) data;
                        String dateString = new SimpleDateFormat("dd MMM yyyy HH:mm").format(date); // 9:00
                        Typeface font = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
                        tv.setTypeface(font);
                        tv.setText(dateString);


                        return true;
                }
            } catch (Exception e) {
//                Log.d("exceptions", "error in adapter view binder: " + e.toString());
            }
            //Log.d(getString(R.string.logtag), "after switch");
            return false;
        }
    }

    class GetWallPostsTask extends GetWallPosts {

        GetWallPostsTask(Object data, SimpleAdapter sAdapter) {
            super(data, sAdapter);

        }

        @Override
        public void onResponseReceived(String result) {
            try {
                //parse response
                VKParser vkParser = new VKParser();
                List<WallPostItem> posts = null;
                if ((result != null) && (!result.equals(""))) {
                    posts = vkParser.parseWall(result);


                    if (isRefresh) {
                        //truncate table
                        dbHandler.truncateNews();
                    }

                    //write to database
                    dbHandler.addNewsPosts(posts);
                } else {
                    //read from db
                    posts = dbHandler.getNewsPosts(postCount - postCountStep, postCountStep);
                    if (posts == null)
                        Toast.makeText(thisActivity, getResources().getString(R.string.no_news), Toast.LENGTH_SHORT).show();
                    //Log.d("mytest", "GOT NEWS POSTS FROM DB: " + (postCount - postCountStep) + " - " + postCountStep);
                }

                if (posts != null) {
                    for (WallPostItem post : posts) {
                        //create new map for a post
                        Map<String, Object> map = new HashMap<String, Object>();
                        map.put(ATTRIBUTE_NAME_TEXT, post.text);
                        PictureItem postPicture = new PictureItem();
                        map.put(ATTRIBUTE_NAME_IMAGE, postPicture);
                        map.put(ATTRIBUTE_NAME_DATE, post.date);

                    /*    if ((post.pictures != null) && (post.pictures.size() != 0)) {
                            //start downloading of pictures

                            String pictureLink = post.pictures.get(0);

                            if (dbHandler.hasPicture(pictureLink)) {
                                Bitmap image = dbHandler.getPicture(pictureLink);
                                int width = image.getWidth();
                                int height = image.getHeight();
                                if (width>800 || height>800){
                                    int threeWidth = width / 2;
                                    int threeHeight = height / 2;
                                    Bitmap bmThree = Bitmap.createScaledBitmap(image, threeWidth,
                                            threeHeight, false);

                                    postPicture.picture = bmThree;}
                                else if (width>500 && width <800 || height>500 && height<800) {
                                    int halfWidth = width;
                                    int halfHeight = height;
                                    Bitmap bmHalf = Bitmap.createScaledBitmap(image, halfWidth, halfHeight, false);
                                    postPicture.picture = bmHalf;
                                } else postPicture.picture = image;
                                //Log.d("mytest", " HAS PICTURE " + pictureLink);
                            } else {
                                DownloadImageTask downloadImageTask = new DownloadImageTask(postPicture){
                                    @Override
                                    public void onResponseReceived(){
                                        //Log.d("mytest", "adding picture to db: " + urldisplay);
                                        if (bmImage.picture != null)
                                            dbHandler.addPicture(bmImage.picture, urldisplay);

                                        sAdapter.notifyDataSetChanged();
                                    };
                                };
                                downloadImageTask.execute(pictureLink);
                            }
                        } */
                        listViewContent.add(map);
                    }
                    sAdapter.notifyDataSetChanged();
                }

                if (isRefresh) {
                    isRefresh = false;
                    lvSimple.setSelectionAfterHeaderView();
                }

            } catch (Exception e) {
//                Log.d("exceptions", "problem in get wall post task after post execute: " + e.toString());
            }
        }
    }
    //new -----------------------------------------------------------
	/* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            switch (position) {
                case 1:
                    //start schedule
                    Intent intent = new Intent(thisActivity, ScheduleActivity.class);
                    thisActivity.finish();
                    startActivity(intent);

                    break;
                case 2:
                    //start schedule
                    Intent intentNews = new Intent(thisActivity, MainActivity.class);
                    thisActivity.finish();
                    startActivity(intentNews);

                    break;
                case 4:
                    Intent iContact = new Intent(thisActivity, ContactsActivity.class);
                    thisActivity.finish();
                    startActivity(iContact);
                    break;
                case 5:
                    Intent intentMap = new Intent(thisActivity, ZoomActivity.class);
                    thisActivity.finish();
                    startActivity(intentMap);
                    break;
                case 6:
                    Intent intentSet = new Intent(thisActivity, Settings.class);
                    thisActivity.finish();
                    startActivity(intentSet);
                    break;
                default:
                    //default menu behavior

                    break;
            }
            // update selected item and title, then close the drawer
//            mDrawerList.setItemChecked(position, true);



            getSupportActionBar().setTitle(R.string.work);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    @Override
    public void setTitle(CharSequence title) {

        mTitle = title;

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        getSupportActionBar().setTitle(R.string.work);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}

