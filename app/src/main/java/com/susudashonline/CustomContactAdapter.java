package com.susudashonline;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by vendetta on 30.08.14.
 */
public class CustomContactAdapter extends BaseAdapter implements Filterable {

    private ArrayList<ContactItem> listData;

    private LayoutInflater layoutInflater;

    private Context mContext;

    private int mPosition;

    // Animation
    Animation slideDown;
    Animation slideUp;


    public CustomContactAdapter(Context context, ArrayList<ContactItem> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
        mContext = context;
        filter = new GameFilter();
        orig = listData;




    }

    @Override
    public int getCount() {
        if(listData != null) {
            return listData.size();
        }
        else return 0;
   }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.contact_item, null);
            holder = new ViewHolder();
            holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
            holder.body = (LinearLayout)convertView.findViewById(R.id.lInfo);
            holder.facultyName = (TextView) convertView.findViewById(R.id.fName);
            holder.mAddress = (TextView) convertView.findViewById(R.id.tvAddress);
            holder.mDekanat = (TextView) convertView.findViewById(R.id.tvDekanat);
            holder.mPhone = (TextView) convertView.findViewById(R.id.tvPhone);
            holder.mEmail = (TextView) convertView.findViewById(R.id.tvEmail);
            holder.mSite = (TextView) convertView.findViewById(R.id.tvSite);
            holder.mSiteTwo = (TextView) convertView.findViewById(R.id.tvSiteTwo);
            holder.mEmailTwo = (TextView) convertView.findViewById(R.id.tvEmailTwo);
            holder.mShedule = (TextView) convertView.findViewById(R.id.tvShedule);
            holder.mTime = (TextView) convertView.findViewById(R.id.tvTime);
            holder.mTime1 = (TextView) convertView.findViewById(R.id.tvTime1);
            holder.mTime2 = (TextView) convertView.findViewById(R.id.tvTime2);
            holder.mTime3 = (TextView) convertView.findViewById(R.id.tvTime3);
            holder.mTime4 = (TextView) convertView.findViewById(R.id.tvTime4);
            holder.llShedule = (LinearLayout)convertView.findViewById(R.id.llShedule);
            holder.mShedule1 = (TextView) convertView.findViewById(R.id.tvShedule1);
            holder.mShedule2 = (TextView) convertView.findViewById(R.id.tvShedule2);
            holder.mShedule3 = (TextView) convertView.findViewById(R.id.tvShedule3);
            holder.mShedule4 = (TextView) convertView.findViewById(R.id.tvShedule4);
            holder.mWorkTime = (TextView) convertView.findViewById(R.id.tvWorkTime);
            holder.mDivider = (ImageView) convertView.findViewById(R.id.divider);
//            holder.mImage = (ImageView) convertView.findViewById(R.id.imgContact);

            convertView.setTag(holder);

            final View finalConvertView = convertView;
            final View finalConvertView1 = convertView;
            holder.name.setOnClickListener( new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v ;
                    holder.body = (LinearLayout) finalConvertView.findViewById(R.id.lInfo);
                    holder.mDivider = (ImageView) finalConvertView1.findViewById(R.id.divider);
                    slideDown = AnimationUtils.loadAnimation(mContext,
                            R.anim.slide_down);
                    slideUp = AnimationUtils.loadAnimation(mContext,
                            R.anim.slide_up);
                    ContactItem itm = (ContactItem) cb.getTag();
                 //   Log.w("Clicked on chckbox",""+cb.getText()+" is "+ cb.isChecked());
                    itm.setSelected(cb.isChecked());


                    if (cb.isChecked()) {
                        holder.body.startAnimation(slideDown);
                        holder.body.setVisibility(View.VISIBLE);
                        holder.mDivider.setVisibility(View.VISIBLE);
                   //     Log.w("set visible","VISIBLE!");

                    } else if (!cb.isChecked()) {
                        holder.body.startAnimation(slideUp);

                     //   Log.w("set gone","gone!");
                    }

                    slideUp.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            holder.body.setVisibility(View.GONE);
                            holder.mDivider.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }
            });

        } else {
            holder = (ViewHolder) convertView.getTag();

        }
        ContactItem contactItem = (ContactItem) listData.get(position);
        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "OpenSans-Regular.ttf");
        holder.facultyName.setTypeface(font);
        holder.facultyName.setText(contactItem.getName());
        holder.mEmail.setVisibility(View.GONE);
        holder.mDekanat.setVisibility(View.GONE);
        holder.mPhone.setVisibility(View.GONE);
        holder.mSite.setVisibility(View.GONE);
        holder.mAddress.setVisibility(View.GONE);
        holder.mSiteTwo.setVisibility(View.GONE);
        holder.mShedule.setVisibility(View.GONE);
        holder.mEmailTwo.setVisibility(View.GONE);
        holder.mTime.setVisibility(View.GONE);
        holder.llShedule.setVisibility(View.GONE);
        holder.mWorkTime.setVisibility(View.GONE);
        holder.body.setVisibility(View.GONE);
        holder.name.setTag(contactItem);
    //    holder.mDivider.setVisibility(View.GONE);



        if (contactItem.getAddress() != null) {
            holder.mAddress.setText("Адрес: " + contactItem.getAddress());
            holder.mAddress.setVisibility(View.VISIBLE);
            holder.mAddress.setTypeface(font);
        }
        if (contactItem.getDekanat() != null) {
            holder.mDekanat.setText("Деканат: " + contactItem.getDekanat());
            holder.mDekanat.setVisibility(View.VISIBLE);
            holder.mDekanat.setTypeface(font);
        }
        if (contactItem.getPhone() != null) {
            holder.mPhone.setText("Телефон: " + contactItem.getPhone());
            holder.mPhone.setVisibility(View.VISIBLE);
            holder.mPhone.setTypeface(font);
        }
        if (contactItem.getEmail() != null) {
            holder.mEmail.setText("Email: " + contactItem.getEmail());
            holder.mEmail.setVisibility(View.VISIBLE);
            holder.mEmail.setTypeface(font);

        }
        if (contactItem.getSite() != null) {
            holder.mSite.setText("Сайт: " + contactItem.getSite());
            holder.mSite.setVisibility(View.VISIBLE);
            holder.mSite.setTypeface(font);
        }

        if (contactItem.getShedule() != null) {
            holder.mShedule.setText("Режим работы: " + contactItem.getShedule());
            holder.mShedule.setVisibility(View.VISIBLE);
            holder.mShedule.setTypeface(font);
        }
        if (contactItem.getTime() != null) {
            holder.mTime.setText("Время работы: " + contactItem.getTime());
            holder.mTime.setVisibility(View.VISIBLE);
            holder.mTime.setTypeface(font);
        }

        if (contactItem.getTime1() != null && contactItem.getTime2() != null && contactItem.getTime3() != null && contactItem.getTime4() != null && contactItem.getShedule1() != null &&
                contactItem.getShedule2() != null && contactItem.getShedule3() != null && contactItem.getShedule4() != null) {
            holder.mWorkTime.setVisibility(View.VISIBLE);
            holder.llShedule.setVisibility(View.VISIBLE);
            holder.mTime1.setText(contactItem.getTime1());
            holder.mTime2.setText(contactItem.getTime2());
            holder.mTime3.setText(contactItem.getTime3());
            holder.mTime4.setText(contactItem.getTime4());
            holder.mShedule1.setText(contactItem.getShedule1());
            holder.mShedule2.setText(contactItem.getShedule2());
            holder.mShedule3.setText(contactItem.getShedule3());
            holder.mShedule4.setText(contactItem.getShedule4());
            holder.mTime1.setTypeface(font);
            holder.mTime2.setTypeface(font);
            holder.mTime3.setTypeface(font);
            holder.mTime4.setTypeface(font);
            holder.mShedule1.setTypeface(font);
            holder.mShedule2.setTypeface(font);
            holder.mShedule3.setTypeface(font);
            holder.mShedule4.setTypeface(font);
        }

//        if (holder.mImage != null) {
//            new ImageDownloaderTask(holder.mImage).execute(contactItem.getImage());
//        }


        return convertView;
    }


        @Override
         public Filter getFilter() {

                return filter;

            }
                 private GameFilter filter;

         ArrayList<ContactItem> orig;


                private class GameFilter extends Filter {

                         public GameFilter() {

                    }
                        @Override

                protected FilterResults performFiltering(CharSequence constraint) {

                        FilterResults oReturn = new FilterResults();

                         ArrayList<ContactItem> results = new ArrayList<ContactItem>();

                         if (orig == null)

                                orig = listData;



                       if (constraint != null)
                            {
                                if (orig != null && orig.size() > 0) {
                                        for (ContactItem g : orig) {
                                                if (g.getName().toLowerCase().contains(constraint) || g.getName().startsWith(constraint.toString()) || g.getName().toUpperCase().contains(constraint)) //contains(constraint))
                                                      results.add(g);
                                          }

                                   }

                                oReturn.values = results;

                            }

                        return oReturn;

                     }



                 @SuppressWarnings("unchecked")

                @Override

                 protected void publishResults(CharSequence constraint, FilterResults results) {

                        listData = (ArrayList<ContactItem>)results.values;

                         notifyDataSetChanged();

                     }

            }




    static class ViewHolder {
        TextView facultyName;
        TextView mAddress;
        TextView mDekanat;
        TextView mPhone;
        TextView mEmail;
        TextView mSite;
        TextView mSiteTwo;
        TextView mEmailTwo;
        TextView mShedule;
        LinearLayout body;
        TextView mTime;
        LinearLayout llShedule;
        TextView mTime1;
        TextView mTime2;
        TextView mTime3;
        TextView mTime4;
        TextView mShedule1;
        TextView mShedule2;
        TextView mShedule3;
        TextView mShedule4;
        TextView mWorkTime;
        ImageView mDivider;
        ImageView mImage;
        CheckBox name;

    }
}