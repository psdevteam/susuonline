package com.susudashonline;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by vendetta on 30.08.14.
 */
public class ContactsActivity extends ActionBarActivity implements Animation.AnimationListener {
    public Field mDragger;
    public Field mEdgeSize;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mNavigationNames;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayList<ContactItem> contactList = null;
    // private ProgressBar progressbar = null;
    private ParallaxListView feedListView = null;
    public CustomContactAdapter adapterC;
    boolean isOpen = true;
    ProgressDialog pd;
    EditText filterText;
    private boolean tapped = false;

    // Animation
    Animation slideDown;
    Animation slideUp;

    private int backgroundNumber = 0;
    private int numberOfBackgrounds = 3;



    protected void onCreate(Bundle saved) {
        super.onCreate(saved);
        setContentView(R.layout.activity_contacts);
        final String url = getResources().getString(R.string.infoUrl);

        if(isOnline()) {
            new DownloadFilesTask().execute(url);
        } else Toast.makeText(ContactsActivity.this,R.string.need_inet,Toast.LENGTH_SHORT).show();



      //  filterText = (EditText) findViewById(R.id.searchText);
      //  filterText.addTextChangedListener(filterTextWatcher);

        mDrawerTitle = getResources().getString(R.string.navigation_title);
        mNavigationNames = getResources().getStringArray(R.array.navigation_names);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mTitle = getResources().getString(R.string.contacts);

        try {
            mDragger = mDrawerLayout.getClass().getDeclaredField("mLeftDragger");//mRightDragger for right obviously
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mDragger.setAccessible(true);
        ViewDragHelper draggerObj = null;
        try {
            draggerObj = (ViewDragHelper) mDragger.get(mDrawerLayout);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            mEdgeSize = draggerObj.getClass().getDeclaredField("mEdgeSize");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (NullPointerException ex){
            ex.printStackTrace();
        }
        mEdgeSize.setAccessible(true);
        int edge = 0;
        try {
            edge = mEdgeSize.getInt(draggerObj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            mEdgeSize.setInt(draggerObj, edge * 12); //optimal value as for me, you may set any constant in dp
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        final Typeface font = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        ArrayAdapter<String> navAdapter = new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mNavigationNames) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                switch (position) {
                    case 0:
                        Drawable draw0 = getResources().getDrawable(R.drawable.ic_action_about);
                        DisplayMetrics metrics = getResources().getDisplayMetrics();
                        int mX = Math.round(50 * metrics.density);
                        draw0.setBounds(0, 0, mX, mX);
                        TextView tw2 = (TextView) view;
                        tw2.setCompoundDrawables(draw0, null, null, null);
                        //Log.d("mytest", tw2.getText().toString());
                        tw2.setTypeface(font);
                        break;

                    case 1:
                        TextView tw1 = (TextView) view;
                        //Log.d("mytest", tw1.getText().toString());
                        Drawable draw1 = getResources().getDrawable(R.drawable.ic_news);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50 * metrics.density);
                        draw1.setBounds(0, 0, mX, mX);
                        tw1.setCompoundDrawables(draw1, null, null, null);
                        tw1.setTypeface(font);

                        break;
                    case 2:
                        TextView tw3 = (TextView) view;
                        //Log.d("mytest", tw3.getText().toString());
                        Drawable draw2 = getResources().getDrawable(R.drawable.ic_work);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50 * metrics.density);
                        draw2.setBounds(0, 0, mX, mX);
                        tw3.setCompoundDrawables(draw2, null, null, null);
                        tw3.setTypeface(font);

                        break;
                    case 3:
                        TextView tw4 = (TextView) view;
                        //Log.d("mytest", tw4.getText().toString());
                        Drawable draw3 = getResources().getDrawable(R.drawable.ic_contacts);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50 * metrics.density);
                        draw3.setBounds(0, 0, mX, mX);
                        tw4.setCompoundDrawables(draw3, null, null, null);
                        tw4.setTypeface(font);

                        break;
                    case 4:
                        TextView tw5 = (TextView) view;
                        //Log.d("mytest", tw5.getText().toString());
                        Drawable draw4 = getResources().getDrawable(R.drawable.ic_map);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50 * metrics.density);
                        draw4.setBounds(0, 0, mX, mX);
                        tw5.setCompoundDrawables(draw4, null, null, null);
                        tw5.setTypeface(font);

                        break;
                    case 5:
                        TextView tw6 = (TextView) view;
                        //Log.d("mytest", tw6.getText().toString());
                        Drawable draw5 = getResources().getDrawable(R.drawable.ic_settings);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50 * metrics.density);
                        draw5.setBounds(0, 0, mX, mX);
                        tw6.setCompoundDrawables(draw5, null, null, null);
                        tw6.setTypeface(font);

                        break;


                    default:
                        break;
                }

                return view;
            }
        };
        View header = (View) getLayoutInflater().inflate(R.layout.logo, null);


        mDrawerList.addHeaderView(header, "logo", false);
        mDrawerList.setAdapter(navAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


        // enable ActionBar app icon to behave as action to toggle nav drawer
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception ex) {
            Log.w("NULL", "icon");
        }
        getSupportActionBar().setHomeButtonEnabled(true);


        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                ContactsActivity.this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.contacts);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.navigation_title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
    }
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 1:
                    try{
                        Intent iShed = new Intent(ContactsActivity.this, ScheduleActivity.class);
                        ContactsActivity.this.finish();
                        startActivity(iShed);
                    } catch (Exception ex){
                        Log.w("Schedule","didn't open");
                    }
                    break;
                case 2:
                    //start schedule
                    try {
                        Intent intentNews = new Intent(ContactsActivity.this, MainActivity.class);
                        ContactsActivity.this.finish();
                        startActivity(intentNews);
                    } catch (Exception ex) {
                        Log.w("MainAct", "didn't open");
                    }
                    break;
                case 3:
                    //start schedule
                    Intent intentWork = new Intent(ContactsActivity.this, WorkActivity.class);
                    ContactsActivity.this.finish();
                    startActivity(intentWork);

                    break;
                case 5:
                    Intent intentMap = new Intent(ContactsActivity.this, ZoomActivity.class);
                    ContactsActivity.this.finish();
                    startActivity(intentMap);
                    break;
                case 6:
                    Intent iSettings = new Intent(ContactsActivity.this, Settings.class);
                    ContactsActivity.this.finish();
                    startActivity(iSettings);
                default:
                    //default menu behavior

                    break;
            }

            // update selected item and title, then close the drawer
//	            mDrawerList.setItemChecked(position, true);
            try {
                //  setTitle(R.string.news);


                getSupportActionBar().setTitle(R.string.sett);

            } catch (Exception ex) {
                Log.w("SecondAct", "don't open");
            }
            mDrawerLayout.closeDrawer(mDrawerList);

        }
    }

    public void setTitle(CharSequence title) {

        mTitle = title;

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else Toast.makeText(ContactsActivity.this, R.string.need_inet, Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        getSupportActionBar().setTitle(R.string.contacts);
        mDrawerToggle.syncState();

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }
   


    

    public void updateList() {
        feedListView = (ParallaxListView) findViewById(R.id.lvContacts);
        feedListView.setVisibility(View.VISIBLE);


        adapterC = new CustomContactAdapter(ContactsActivity.this, contactList);

        EditText v = new EditText(this);
        v.setHint("Поиск");
        v.setGravity(Gravity.CENTER);
        v.setTextSize(20);
        v.setHeight(100);
        v.setTextColor(Color.parseColor("#ffffff"));
        v.setBackgroundColor(Color.parseColor("#CC2d1816"));
        v.addTextChangedListener(filterTextWatcher);
        feedListView.addParallaxedHeaderView(v);

        feedListView.setAdapter(adapterC);


        feedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, final View view, int position, long arg) {
                TextView v = (TextView) view.findViewById(R.id.fName);
                LinearLayout body = (LinearLayout) view.findViewById(R.id.lInfo);
                ImageView div = (ImageView) view.findViewById(R.id.divider);
                CheckBox cb = (CheckBox) v.findViewById(R.id.checkBox1);

                slideDown = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide_down);
                slideUp = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide_up);
                // set animation listener
                slideDown.setAnimationListener(ContactsActivity.this);
                slideUp.setAnimationListener(ContactsActivity.this);
           /*    switch(body.getVisibility()){
                    case 8: {
                        body.startAnimation(slideDown);
                        body.setVisibility(View.VISIBLE);
                        div.setVisibility(View.VISIBLE);
                        Log.w("body vis",""+body.getVisibility());
                        Log.w("set visible","VISIBLE!");
                    }
                    break;
                   case 0:{
                       body.startAnimation(slideUp);
                       body.setVisibility(View.GONE);
                       div.setVisibility(View.GONE);
                       Log.w("body vis",""+body.getVisibility());
                       Log.w("set gone","GONE!");
                   }
                   break;

                  }
               */


            }
        });
    }





    // animation listeners
    @Override
    public void onAnimationEnd(Animation animation) {
        // Take any action after completing the animation
        // check for fade in animation
        if (animation == slideDown) {
//            Toast.makeText(getApplicationContext(), "Animation Stopped",
//                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // Animation is repeating
    }

    @Override
    public void onAnimationStart(Animation animation) {
        // Animation started
    }

    private class DownloadFilesTask extends AsyncTask<String, Integer, Void> {

        @Override
        protected  void onPreExecute(){
            super.onPreExecute();
            pd = new ProgressDialog(ContactsActivity.this);
            pd.show();
            pd.setTitle("Загрузка");
            pd.setMessage("Подождите");
            pd.setCancelable(false);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Void result) {
            if (null != contactList) {
                updateList();

            }
            pd.dismiss();

        }

        @Override
        protected Void doInBackground(String... params) {
            String url = params[0];

// getting JSON string from URL
            JSONObject json = getJSONFromUrl(url);

//parsing json data
            parseJson(json);
            return null;
        }
    }


    public JSONObject getJSONFromUrl(String url) {
        InputStream is = null;
        JSONObject jObj = null;
        String json = null;

// Making HTTP request
        try {
// defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

// return JSON String
        return jObj;

    }

    public void parseJson(JSONObject json) {
        try {

// parsing json object
            //   if (json.getString("status").equalsIgnoreCase("ok")) {
            JSONArray posts = json.getJSONArray("faculties");
            Log.w("posts",""+posts);
            contactList = new ArrayList<ContactItem>();
            Log.w("LENGTH", "" + posts.length());
            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = (JSONObject) posts.getJSONObject(i);
                ContactItem item = new ContactItem();
                item.setName(post.getString("name"));
                item.setAddress(post.getString("address"));
                if(!post.getString("dekanat").equalsIgnoreCase("") && post.getString("dekanat")!=null) {
                    item.setDekanat(post.getString("dekanat"));
                }
                if(!post.getString("email").equalsIgnoreCase("") && post.getString("email") != null) {
                    item.setEmail(post.getString("email"));
                }
                if (!post.getString("site").equalsIgnoreCase("") && post.getString("site") != null) {
                    item.setSite(post.getString("site"));
                }
                if (post.has("shedule") && !post.getString("shedule").equalsIgnoreCase("") && post.getString("shedule") != null){
                    item.setShedule(post.getString("shedule"));
                }
                if(post.has("time") && !post.getString("time").equalsIgnoreCase("") && post.getString("time")!= null){
                    item.setTime(post.getString("time"));
                }

//                if(post.has("image")){
//                    item.setImage(post.getString("image"));
//                }

                if (post.has("time1") && post.has("shedule1") && post.has("time2") && post.has("shedule2") && post.has("time3") && post.has("shedule3") && post.has("time4") && post.has("shedule4")){
                    if(!post.getString("time1").equalsIgnoreCase("") &&!post.getString("shedule1").equalsIgnoreCase("") && !post.getString("time2").equalsIgnoreCase("") && !post.getString("shedule2").equalsIgnoreCase("") && !post.getString("time3").equalsIgnoreCase("shedule3") && !post.getString("time4").equalsIgnoreCase("") && !post.getString("shedule4").equalsIgnoreCase("") ){
                        item.setTime1(post.getString("time1"));
                        item.setTime2(post.getString("time2"));
                        item.setTime3(post.getString("time3"));
                        item.setTime4(post.getString("time4"));
                        item.setShedule1(post.getString("shedule1"));
                        item.setShedule2(post.getString("shedule2"));
                        item.setShedule3(post.getString("shedule3"));
                        item.setShedule4(post.getString("shedule4"));

                    }
                }
                contactList.add(item);
            }
            //  }
        } catch (JSONException e) {
            e.printStackTrace();


        }

    }


    @Override
    public void onBackPressed() {

        // Завершаем наше activity
           this.finish();


    }

    private TextWatcher filterTextWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            adapterC.getFilter().filter(s);
        }

    };

}

