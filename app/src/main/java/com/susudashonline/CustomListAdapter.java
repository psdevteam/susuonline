package com.susudashonline;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

/**
 * Created by vendetta on 28.08.14.
 */
public class CustomListAdapter extends BaseAdapter {

    private AQuery aq;

    private ArrayList<FeedItem> listData;

    private LayoutInflater layoutInflater;

    private Context mContext;


    public CustomListAdapter(Context context, ArrayList<FeedItem> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
        mContext = context;
        this.aq = new AQuery(context);

    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.headlineView = (TextView) convertView.findViewById(R.id.titleNews);
            holder.reportedDateView = (TextView) convertView.findViewById(R.id.date2News);
            holder.imageView = (com.makeramen.RoundedImageView) convertView.findViewById(R.id.thumbImageNews);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        FeedItem newsItem = (FeedItem) listData.get(position);
        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "OpenSans-Regular.ttf");
        holder.headlineView.setTypeface(font);
        holder.headlineView.setText(newsItem.getTitle());
        holder.reportedDateView.setText(newsItem.getDate());

        if (holder.imageView != null) {
//            new ImageDownloaderTask(holder.imageView).execute(newsItem.getAttachmentUrl());
            // загружаем картинку в ImageView из сети

            aq.id(holder.imageView).image(
                    newsItem.getAttachmentUrl(),true,true,0,0,null,0);


        }

        return convertView;
    }

    static class ViewHolder {
        TextView headlineView;
        TextView reportedDateView;
        com.makeramen.RoundedImageView imageView;
    }
}