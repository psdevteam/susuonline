package com.susudashonline;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;





public class ScheduleActivity extends ActionBarActivity  {
	private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mNavigationNames;
	
	int termNumber;
	int termPart;
	int weekNumber;
	//Activity ScheduleActivity.this = this;
	Spinner facultySpinner;
	Spinner groupSpinner;
	Switch weekSwitch;
	List<FacultyItem> facultyList;
	List<GroupItem> groupList;
	List<ClassItem> schedule;
	List<String> groupNumbers;
	List<String> facultyNames;
	List<CharSequence> weekDays;
	ArrayAdapter<String> facultyAdapter;
	ArrayAdapter<String> groupAdapter;
	Button getButton;
	List<CharSequence> allowedFaculties;
	GroupItem selectedGroup;
	FacultyItem selectedFaculty;
	DBHandler dbHandler;
    public Field mDragger;
    public Field mEdgeSize;

   


   	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_layout);
        Intent intent = getIntent();


        

        getSupportActionBar().setTitle(R.string.title_activity_schedule);

  



		
		//create database handler
		dbHandler = new DBHandler(this);
		
		//find spinners and buttons
		//facultyNames = new ArrayList<String>();
		//groupNumbers = new ArrayList<String>();
		weekDays = Arrays.asList(getResources().getTextArray(R.array.weekdays));
		//facultyNames.add(getResources().getString(R.string.space));
		allowedFaculties = Arrays.asList(getResources().getTextArray(R.array.allowed_faculties));
		//facultySpinner = (Spinner)  findViewById(R.id.faculty_spinner);
	//	groupSpinner = (Spinner)  findViewById(R.id.group_spinner);
	//	getButton = (Button)  findViewById(R.id.schedule_button);
//		weekSwitch = (Switch)findViewById(R.id.week_switch);
		mTitle = getResources().getString(R.string.title_activity_schedule);
		
		//basic offline movements
		Map<String, String> currentParameters = dbHandler.getCurrentParameters();
		if (currentParameters == null) {
            if(isOnline()) {
                Toast.makeText(this, getResources().getString(R.string.need_sett), Toast.LENGTH_SHORT).show();
            } else Toast.makeText(ScheduleActivity.this,R.string.need_inet,Toast.LENGTH_SHORT).show();
        }
		else {

			selectedGroup = dbHandler.getGroup(currentParameters.get("group_id"));
			selectedFaculty = dbHandler.getFacultyOfGroup(selectedGroup);
		
			Calendar calendar = Calendar.getInstance(); 
			weekNumber = (calendar.get(Calendar.WEEK_OF_YEAR) % 2) + 1;
			
			try {
				termNumber = Integer.parseInt(currentParameters.get("term_number"));
				termPart = Integer.parseInt(currentParameters.get("term_part"));
			} catch (Exception e) {
//				Log.d("exceptions", "error parsing term number and part from current parameters from database");
			}
		//	Log.d("mytest", "getting classes for group: " + selectedGroup.groupNumber + " term part: " + termPart + "term number: " + termNumber);
			schedule = dbHandler.getClasses(selectedGroup, termPart, termNumber);
			
			//call method that fills the table (schedule, termNumber, termPart, weekNumber)
			try {
				this.fillScheduleTable(schedule, termNumber, termPart, weekNumber);
			} catch (Exception e) {
//				Log.d("exceptions", "error filling schedule from offline database");
			}


		
		}




		//-------------------------------------------------------------------------------
	    mDrawerTitle = getResources().getString(R.string.navigation_title);
        mNavigationNames = getResources().getStringArray(R.array.navigation_names);
        mDrawerLayout = (DrawerLayout)  findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)  findViewById(R.id.left_drawer);


        try {
            mDragger = mDrawerLayout.getClass().getDeclaredField("mLeftDragger");//mRightDragger for right obviously
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mDragger.setAccessible(true);
        ViewDragHelper draggerObj = null;
        try {
            draggerObj = (ViewDragHelper) mDragger.get(mDrawerLayout);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            mEdgeSize = draggerObj.getClass().getDeclaredField("mEdgeSize");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mEdgeSize.setAccessible(true);
        int edge = 0;
        try {
            edge = mEdgeSize.getInt(draggerObj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            mEdgeSize.setInt(draggerObj, edge * 12); //optimal value as for me, you may set any constant in dp
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // set a custom shadow that overlays the main content when the drawer opens
        // set up the drawer's list view with items and click listener

        final Typeface font = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        ArrayAdapter<String> navAdapter = new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mNavigationNames) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                switch (position) {
                    case 0:
                        Drawable draw0 = getResources().getDrawable(R.drawable.ic_action_about);
                        DisplayMetrics metrics = getResources().getDisplayMetrics();
                        int mX = Math.round(50*metrics.density);
                        draw0.setBounds(0, 0, mX, mX);
                        TextView tw2 = (TextView) view;
                        tw2.setCompoundDrawables(draw0, null, null, null);
//                        Log.d("mytest", tw2.getText().toString());
                        tw2.setTypeface(font);
                        break;

                    case 1:
                        TextView tw1 = (TextView) view ;
//                        Log.d("mytest", tw1.getText().toString());
                        Drawable draw1 = getResources().getDrawable(R.drawable.ic_news);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw1.setBounds(0, 0, mX, mX);
                        tw1.setCompoundDrawables(draw1, null, null, null);
                        tw1.setTypeface(font);

                        break;
                    case 2:
                        TextView tw3 = (TextView) view;
//                        Log.d("mytest", tw3.getText().toString());
                        Drawable draw2 = getResources().getDrawable(R.drawable.ic_work);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw2.setBounds(0, 0, mX, mX);
                        tw3.setCompoundDrawables(draw2, null, null, null);
                        tw3.setTypeface(font);

                        break;
                    case 3:
                        TextView tw4 = (TextView) view;
//                        Log.d("mytest", tw4.getText().toString());
                        Drawable draw3 = getResources().getDrawable(R.drawable.ic_contacts);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw3.setBounds(0, 0, mX, mX);
                        tw4.setCompoundDrawables(draw3, null, null, null);
                        tw4.setTypeface(font);

                        break;
                    case 4:
                        TextView tw5 = (TextView) view;
//                        Log.d("mytest", tw5.getText().toString());
                        Drawable draw4 = getResources().getDrawable(R.drawable.ic_map);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw4.setBounds(0, 0, mX, mX);
                        tw5.setCompoundDrawables(draw4, null, null, null);
                        tw5.setTypeface(font);

                        break;
                    case 5:
                        TextView tw6 = (TextView) view;
//                        Log.d("mytest", tw6.getText().toString());
                        Drawable draw5 = getResources().getDrawable(R.drawable.ic_settings);
                        metrics = getResources().getDisplayMetrics();
                        mX = Math.round(50*metrics.density);
                        draw5.setBounds(0, 0, mX, mX);
                        tw6.setCompoundDrawables(draw5, null, null, null);
                        tw6.setTypeface(font);

                        break;


                    default:
                        break;
                }

                return view;
            }
        };
        View header = (View)getLayoutInflater().inflate(R.layout.logo,null);
        mDrawerList.addHeaderView(header,"logo",false);
        mDrawerList.setAdapter(navAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
//        TextView rasp = (TextView)findViewById(R.id.schedule_header);
//        Typeface fontSched = Typeface.createFromAsset(getAssets(),"OpenSans-Regular.ttf");
        TextView lDraw = (TextView) findViewById(R.id.leftDrawItem);
        Typeface fontDraw = Typeface.createFromAsset( getAssets(), "OpenSans-Bold.ttf");
        try {
            lDraw.setTypeface(fontDraw);
        } catch (Exception e){
            Log.w("WARN!","shit");
        }

        // enable ActionBar app icon to behave as action to toggle nav drawer
        try {

             getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e){
            Log.i("Attention!","NullPointerException!");
        }
         getSupportActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
            	super.onDrawerClosed(view);
                try {

                   getSupportActionBar().setTitle(R.string.title_activity_schedule);
                } catch (Exception ex){
                    Log.w("SecondAct","don't open");
                }
                 invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView){
            	super.onDrawerOpened(drawerView);
                try {


                     getSupportActionBar().setTitle(R.string.navigation_title);
                     invalidateOptionsMenu();
                } catch (Exception e){
                    Log.w("WARN!","shit error");
                }
                 // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
//        if (savedInstanceState == null) {
//            selectItem(0);
//        }
        //-------------------------------------------------------------------------------------------------
      
	}

	
	public void fillScheduleTable(List<ClassItem> schedule, int termNumber, int termPart, int weekNumber) throws Exception {
		//get layout inflater
		LayoutInflater ltInflater =  getLayoutInflater();
		
		//clean the table
		for (CharSequence day : weekDays) {
    		for (int i = 1; i < 7; i++) { // внесены изменения
    			int rowId = getResources().getIdentifier(day.toString() + i, "id",  getPackageName());
            //    Log.w("rowId",""+String.valueOf(rowId));
    			if (rowId == 0) 
    				throw new Exception("table row was not found for timestring :" + day + i);
    			
    			int headerId = getResources().getIdentifier(day + "_header", "id",  getPackageName());
    			if (headerId == 0) 
    				throw new Exception("table row header was not found for " + day);
    			
    			TableRow tableRow = (TableRow)  findViewById(rowId);
    			tableRow.removeAllViews();
    			
    			TableRow tableHeaderRow = (TableRow)  findViewById(headerId);
    			tableHeaderRow.setVisibility(View.GONE);
    		}
		}

		//walk through whole list
		for (ClassItem classItem : schedule) {
			String timeString = "";
			
			if (((classItem.weekNumber == weekNumber) | (classItem.weekNumber == 0)) && 
					(classItem.termPart == termPart) && (classItem.termNumber == termNumber)) {
				timeString = weekDays.get(classItem.weekDay - 1).toString() + classItem.pairNumber;

              //  Log.w("timeString",""+timeString);
				
				//make day header visible
				String day = weekDays.get(classItem.weekDay - 1).toString();
              //  Log.w("day",day);
				int headerId = getResources().getIdentifier(day + "_header", "id",  getPackageName());
				if (headerId == 0) 
					throw new Exception("table row header was not found for " + day);
				
				TableRow tableHeaderRow = (TableRow)  findViewById(headerId);
				tableHeaderRow.setVisibility(View.VISIBLE);
				
				//get table row for this class
				int rowId = getResources().getIdentifier(timeString, "id",  getPackageName());
				if (rowId == 0) 
					throw new Exception("table row was not found for timestring :" + timeString);
				
				TableRow tableRow = (TableRow)  findViewById(rowId);
				//set row on click listener
				tableRow.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick (View view) {
						TextView classId = (TextView) view.findViewById(R.id.class_id);
						
						//find class from schedule by id
						ClassItem cl = findClassById(classId.getText().toString());
						if (cl != null) {
							//show dialog with class info
							ClassDialog dialog = (new ClassDialog()).newInstance(ScheduleActivity.this, cl);
							FragmentTransaction ft = getFragmentManager().beginTransaction();
							dialog.show(ft, "class_info_dialog");
						}
					}
				});
				
				TextView classTime = (TextView) ltInflater.inflate(R.layout.class_time, null, false);
				TextView classSubject = (TextView) ltInflater.inflate(R.layout.class_subject, null, false);
				TextView classId = (TextView) ltInflater.inflate(R.layout.class_id, null, false);
                Typeface font = Typeface.createFromAsset( getAssets(), "OpenSans-Bold.ttf");
                Typeface fontText = Typeface.createFromAsset( getAssets(), "OpenSans-Regular.ttf");

                TextView monday = (TextView)findViewById(R.id.mon);
                TextView tuesday = (TextView)findViewById(R.id.tue);
                TextView wednesday = (TextView)findViewById(R.id.wed);
                TextView thursday = (TextView)findViewById(R.id.thu);
                TextView friday = (TextView)findViewById(R.id.fri);
                TextView saturday = (TextView)findViewById(R.id.sat);


                monday.setTypeface(fontText);
                tuesday.setTypeface(fontText);
                wednesday.setTypeface(fontText);
                thursday.setTypeface(fontText);
                friday.setTypeface(fontText);
                saturday.setTypeface(fontText);

                classTime.setTypeface(font);
                classSubject.setTypeface(fontText);




				
				//make time properly formatted for displaying
				int beginHours = Integer.parseInt(classItem.beginTime.substring(classItem.beginTime.indexOf("PT") + 2, classItem.beginTime.indexOf("H")));
				
				int beginMinutes = 0;

				if (classItem.beginTime.indexOf("M") != -1)
					beginMinutes = Integer.parseInt(classItem.beginTime.substring(classItem.beginTime.indexOf("H") + 1, classItem.beginTime.indexOf("M")));

				DecimalFormat myFormatter = new DecimalFormat("00");
				classTime.setText("" + myFormatter.format(beginHours) + ":" 
									+ myFormatter.format(beginMinutes)
								);
             //   Log.w("classTime",classTime.getText().toString());
				classSubject.setText(classItem.subject + " " + classItem.room + " / " + classItem.building);
             //   Log.w("classSubject",classSubject.getText().toString());
				classId.setText(classItem.id);
				
				tableRow.addView(classTime);
				tableRow.addView(classSubject);
				tableRow.addView(classId);
				classId.setVisibility(View.GONE);
			}
		}
	}
	
	//new -----------------------------------------------------------
		/* The click listner for ListView in the navigation drawer */
	    private class DrawerItemClickListener implements ListView.OnItemClickListener {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	        	switch (position) {
	        	case 2:
	        		//start schedule
                    try {
                        Intent intent = new Intent(ScheduleActivity.this, MainActivity.class);
                        ScheduleActivity.this.finish();
                        startActivity(intent);
                    } catch (Exception ex){
                        Log.w("SecondAct","don't open");
                    }
	        		break;
                    case 3:
                        Intent intentWork = new Intent(ScheduleActivity.this, WorkActivity.class);
                        ScheduleActivity.this.finish();
                        startActivity(intentWork);
                        break;
                    case 4:
                        Intent iContact = new Intent(ScheduleActivity.this, ContactsActivity.class);
                        ScheduleActivity.this.finish();
                        startActivity(iContact);
                        break;
                    case 5:
                        Intent intentMap = new Intent(ScheduleActivity.this, ZoomActivity.class);
                        ScheduleActivity.this.finish();
                        startActivity(intentMap);
                        break;
                    case 6:
                        try {
                            Intent intent = new Intent(ScheduleActivity.this, Settings.class);
                            ScheduleActivity.this.finish();
                            startActivity(intent);
                        } catch (Exception ex){
                            Log.w("Settings","don't open");
                        }
                        break;

	    		default:
	    			//default menu behavior
	    			
	    			break;
	        	}
	        	
	        	// update selected item and title, then close the drawer
//	            mDrawerList.setItemChecked(position, true);
                try {
                   //  setTitle(R.string.news);


                    getSupportActionBar().setTitle(R.string.title_activity_schedule);

                 } catch (Exception ex){
            Log.w("SecondAct","don't open");
             }
	            mDrawerLayout.closeDrawer(mDrawerList);

	        }
	    }


	    public void setTitle(CharSequence title) {

	        mTitle = title;
            try {

                getSupportActionBar().setTitle(R.string.title_activity_schedule);
            } catch (Exception ex){
//                Log.d("WARN","one more catched");
            }
	    }


        @Override
	    protected void onPostCreate(Bundle savedInstanceState) {
	        super.onPostCreate(savedInstanceState);
	        // Sync the toggle state after onRestoreInstanceState has occurred.
            mDrawerToggle.syncState();

	    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        Switch weekSwitch = (Switch)menu.findItem(R.id.mySwitch).getActionView().findViewById(R.id.week_switch);
        weekSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    weekNumber = 1;
                } else {
                    weekNumber = 2;
                }
                try {
                    if (schedule != null) {
		        	Log.w("SHEDULE SWITCH", "filling schedule table. week=" + weekNumber + " termpart=" + termPart + " termNumber=" + termNumber);
                        fillScheduleTable(schedule, termNumber, termPart, weekNumber);
                    } else {
                        Toast.makeText(ScheduleActivity.this, getResources().getString(R.string.no_classes), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
//                    Log.d("exceptions", "after week change: " + e.toString());
                }
            }
        });
        if (weekNumber == 1)
            weekSwitch.setChecked(true);
        else
            weekSwitch.setChecked(false);


        return super.onCreateOptionsMenu(menu);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else Toast.makeText(ScheduleActivity.this,R.string.need_inet,Toast.LENGTH_SHORT).show();
        return false;
    }
	    @Override
	    public void onConfigurationChanged(Configuration newConfig) {
	        super.onConfigurationChanged(newConfig);
	        // Pass any configuration change to the drawer toggls
	        mDrawerToggle.onConfigurationChanged(newConfig);
	    }
	    
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        // Pass the event to ActionBarDrawerToggle, if it returns
	        // true, then it has handled the app icon touch event
	        if (mDrawerToggle.onOptionsItemSelected(item)) {
	          return true;
	        }
	        // Handle your other action bar items...
            switch (item.getItemId())
            {
                case R.id.week_switch:


                  //  Toast.makeText(this, "here", Toast.LENGTH_SHORT).show();
                    break;

                default:
                    break;
            }



	        return super.onOptionsItemSelected(item);
	    }


	    //find class from schedule by class id
	    public ClassItem findClassById(String id) {
	    	for (ClassItem classItem : schedule) {
	    		if (classItem.id.equals(id))
	    			return classItem;
	    	}
	    	
	    	return null;
	    }
}
